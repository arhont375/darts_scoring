﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Windows.Input;
using System.Windows;

using Darts;

//This code from StackOverflow (http://stackoverflow.com/questions/10216757/adding-inputbox-like-control-to-xna-game)
//Also in some places modified by me.
namespace Darts.EventInput
{
    //Интерфейс класса для подписчика на события клавиатуры
    public interface IKeyboardSubscriber
    {
        void RecieveTextInput(char inputChar);
        void RecieveTextInput(string text);
        void RecieveCommandInput(char command);
        void RecieveSpecialInput(Keys key);

        bool Selected { get; set; } //or Focused
    }

    public class KeyboardDispatcher
    {
        //Инициализация
        public KeyboardDispatcher(GameWindow window)
        {
            EventInput.Initialize(window);
            //Подписка на события
            EventInput.CharEntered += new CharEnteredHandler(EventInput_CharEntered);
            EventInput.KeyDown += new KeyEventHandler(EventInput_KeyDown);
        }

        /// <summary>
        /// Присвоение нового подписчика, который получает ввод
        /// </summary>
        IKeyboardSubscriber _subscriber;
        public IKeyboardSubscriber Subscriber
        {
            get { return _subscriber; }
            set
            {
                if (_subscriber != null)
                    _subscriber.Selected = false;
                _subscriber = value;
                if (value != null)
                    value.Selected = true;
            }
        }
        /// <summary>
        /// Отписывает выбранное ранее поле от получения символов
        /// </summary>
        /// <param name="sender"></param>
        public void TextBox_UnSubscriber(TextBox.TextBox sender)
        {
            if (_subscriber == sender)
            {
                _subscriber.Selected = false;
                _subscriber = null;
            }
        }
        /// <summary>
        /// Передаёт события клавиатуры подписчикам, если они есть
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void EventInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (_subscriber == null)
                return;

            _subscriber.RecieveSpecialInput(e.KeyCode);
        }


        /// <summary>
        /// Обработка вставки из clipboard 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void EventInput_CharEntered(object sender, CharacterEventArgs e)
        {
            if (_subscriber == null)
                return;
            if (char.IsControl(e.Character))
            {
                //ctrl-v
                if (e.Character == 0x16)
                {
                    //XNA runs in Multiple Thread Apartment state, which cannot recieve clipboard
                    Thread thread = new Thread(PasteThread);
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                    thread.Join();
                    _subscriber.RecieveTextInput(_pasteResult);
                }
                else
                {
                    _subscriber.RecieveCommandInput(e.Character);
                }
            }
            else
            {
                _subscriber.RecieveTextInput(e.Character);
            }
        }

        /// <summary>
        /// Смена активного поля
        /// </summary>
        /// <param name="sender"></param>
        public void TextBox_Clicked(TextBox.TextBox sender)
        {
            this.Subscriber = sender;
        }
        //Thread has to be in Single Thread Apartment state in order to receive clipboard
        string _pasteResult = "";
        [STAThread]
        void PasteThread()
        {
            if (Clipboard.ContainsText())
            {
                _pasteResult = Clipboard.GetText();
            }
            else
            {
                _pasteResult = "";
            }
        }
    }
}
