﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Darts.Text
{
    class Table 
    {
        int numberOfRows;
        /// <summary>
        /// Cols for each players + extra
        /// </summary>
        List<List <TextBox.TextBox>> baseTable;
        TextBox.TextBox[] lastRow;

        SpriteFont font;
        Texture2D textureTextField;
        Texture2D textureCarret;

        Vector2 positionCenter;
        Vector2 position;
        Vector2 positionShift;
        Vector2 shiftFirstCol;

        static Darts.TextBox.TextBox currentTextField;
        static Darts.TextBox.TextBox previousTextField;
        static Darts.TextBox.TextBox currentMoveField;
        static Darts.TextBox.TextBox currentPlayerField;

        int currentNumberOfPlayers;
        int textFieldLength = 105;
        int constTextFieldLength = 35;
        int initScore = 0;
        float scope = 1;
        string defaultString = "-";

        public Table(Vector2 _centerPosition, Texture2D _textureTextFeild, Texture2D _textureCarret, SpriteFont _font, float _scope)
        {
            positionCenter = _centerPosition;
            textureCarret = _textureCarret;
            textureTextField = _textureTextFeild;
            font = _font;

            Initialize();
        }

        void InitCol(List<TextBox.TextBox> col, int _value = 0)
        {
            for (int row = 1; row < numberOfRows; ++row)
            {
                col[row].Text = defaultString;
                col[row].ValuesNumber = _value;
                col[row].IsReadOnly = true;
            }
        }

        public void Initialize()
        {
            baseTable = new List<List<TextBox.TextBox>>(Game1.MAX_PLAYERS + 1);
            
            lastRow = new TextBox.TextBox[Game1.MAX_PLAYERS + 1];

            currentNumberOfPlayers = Game1.NumberOfPlayers;

            numberOfRows = 20 + 1;
            position = new Vector2(positionCenter.X - (textFieldLength * currentNumberOfPlayers + constTextFieldLength) * scope / 2, 
                                    positionCenter.Y - textureTextField.Height / 2 * scope * numberOfRows / 2);



            IsActive = true;
            IsPreparedForCricket = false;

            Vector2 currentPosition;
            currentPosition = position;


            positionShift = new Vector2(textFieldLength * scope, textureTextField.Height * scope / 2);
            shiftFirstCol = new Vector2(constTextFieldLength * scope, textureTextField.Height * scope / 2);  

            baseTable.Add(new List<TextBox.TextBox>(numberOfRows));
            baseTable.Add(new List<TextBox.TextBox>(numberOfRows));
            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[0].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
                baseTable[0][row].IsReadOnly = true;
                baseTable[0][row].Width = constTextFieldLength;
                baseTable[0][row].X = (int)currentPosition.X;
                baseTable[0][row].Y = (int)currentPosition.Y;
                baseTable[0][row].Text = row.ToString(); 
                currentPosition.Y += positionShift.Y;
            }
            baseTable[0][0].Text = " #";

            lastRow[0] = new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher);
            lastRow[0].IsReadOnly = true;
            lastRow[0].Width = constTextFieldLength;
            lastRow[0].X = (int)currentPosition.X;
            lastRow[0].Y = (int)currentPosition.Y;
            lastRow[0].Text = "∑";

            currentPosition.X += shiftFirstCol.X;
            currentPosition.Y = position.Y;
            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[1].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
                baseTable[1][row].Width = textFieldLength;
                baseTable[1][row].X = (int)currentPosition.X;
                baseTable[1][row].Y = (int)currentPosition.Y;
                currentPosition.Y += positionShift.Y;
            }
            baseTable[1][0].Text = "Name1";

            lastRow[1] = new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher);
            lastRow[1].IsReadOnly = true;
            lastRow[1].Width = textFieldLength;
            lastRow[1].X = (int)currentPosition.X;
            lastRow[1].Y = (int)currentPosition.Y;
            lastRow[1].Text = initScore.ToString();
            lastRow[0].Text = " ∑";

            currentTextField = baseTable[1][1];
            currentTextField.IsActive = true;
            InitCol(baseTable[1]);

            currentMoveField = baseTable[0][1];
            currentMoveField.Highlighted = true;
            currentPlayerField = baseTable[1][0];
            currentPlayerField.IsActive = true;
        }

        void AddNewPlayer()
        {
            position = new Vector2(positionCenter.X - (textFieldLength * Game1.NumberOfPlayers + constTextFieldLength) * scope / 2, positionCenter.Y - textureTextField.Height / 2 * scope * numberOfRows / 2);

            Vector2 currentPosition;
            currentPosition = position;

            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[0][row].X = (int)currentPosition.X;
                baseTable[0][row].Y = (int)currentPosition.Y;
                currentPosition.Y += positionShift.Y;
            }
            lastRow[0].X = (int)currentPosition.X;
            lastRow[0].Y = (int)currentPosition.Y;

            currentPosition.X += shiftFirstCol.X;
            currentPosition.Y = position.Y;
            for (int col = 1; col <= currentNumberOfPlayers; ++col) 
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    baseTable[col][row].X = (int)currentPosition.X;
                    baseTable[col][row].Y = (int)currentPosition.Y;
                    currentPosition.Y += positionShift.Y;
                }
                lastRow[col].X = (int)currentPosition.X;
                lastRow[col].Y = (int)currentPosition.Y;

                currentPosition.X += positionShift.X;
                currentPosition.Y = position.Y;
            }
            baseTable.Add(new List<TextBox.TextBox>(numberOfRows));
            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[Game1.NumberOfPlayers].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
                baseTable[Game1.NumberOfPlayers][row].Width = textFieldLength;
                baseTable[Game1.NumberOfPlayers][row].X = (int)currentPosition.X;
                baseTable[Game1.NumberOfPlayers][row].Y = (int)currentPosition.Y;
                currentPosition.Y += positionShift.Y;
            }
            lastRow[Game1.NumberOfPlayers] = new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher);
            lastRow[Game1.NumberOfPlayers].IsReadOnly = true;
            lastRow[Game1.NumberOfPlayers].Width = textFieldLength;
            lastRow[Game1.NumberOfPlayers].X = (int)currentPosition.X;
            lastRow[Game1.NumberOfPlayers].Y = (int)currentPosition.Y;
            lastRow[Game1.NumberOfPlayers].Text = initScore.ToString();    
            baseTable[Game1.NumberOfPlayers][0].Text = "Name" + Game1.NumberOfPlayers.ToString();

            InitCol(baseTable[Game1.NumberOfPlayers]);
            InitLastRow(initScore);
        }

        void DeleteLastPlayer()
        {
            position = new Vector2(positionCenter.X - (textFieldLength * Game1.NumberOfPlayers + constTextFieldLength) * scope / 2, positionCenter.Y - textureTextField.Height / 2 * scope * numberOfRows / 2);

            Vector2 currentPosition;
            currentPosition = position;

            baseTable.RemoveAt(currentNumberOfPlayers);
            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[0][row].X = (int)currentPosition.X;
                baseTable[0][row].Y = (int)currentPosition.Y;
                currentPosition.Y += positionShift.Y;
            }
            lastRow[0].X = (int)currentPosition.X;
            lastRow[0].Y = (int)currentPosition.Y;

            currentPosition.X += shiftFirstCol.X;
            currentPosition.Y = position.Y;
            for (int col = 1; col < currentNumberOfPlayers; ++col)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    baseTable[col][row].X = (int)currentPosition.X;
                    baseTable[col][row].Y = (int)currentPosition.Y;
                    currentPosition.Y += positionShift.Y;
                }
                lastRow[col].X = (int)currentPosition.X;
                lastRow[col].Y = (int)currentPosition.Y;

                currentPosition.X += positionShift.X;
                currentPosition.Y = position.Y;
            } 
        }

        void AddSomeRows(int newRows = 1, int numberOfNewRow = 0)
        {
            int newNumberOfRows = numberOfRows + newRows;
            Vector2 newPosition = new Vector2(positionCenter.X - (textFieldLength * currentNumberOfPlayers + constTextFieldLength) * scope / 2,
                                                positionCenter.Y - textureTextField.Height / 2 * scope * newNumberOfRows / 2);
          

            //add new field for first column
            baseTable[0].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
            baseTable[0][numberOfRows].IsReadOnly = true;
            baseTable[0][numberOfRows].Width = constTextFieldLength;
            if (numberOfNewRow == 0)
            {
                baseTable[0][numberOfRows].Text = numberOfRows.ToString();
            }
            else
            {
                baseTable[0][numberOfRows].Text = numberOfNewRow.ToString();
            }
            //add new fields for others columns
            for (int i = 1; i <= Game1.NumberOfPlayers; ++i)
            {
                baseTable[i].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
                baseTable[i][numberOfRows].IsReadOnly = true;
                baseTable[i][numberOfRows].Width = textFieldLength;
                baseTable[i][numberOfRows].Text = defaultString;
            }

            //set position for fields
            Vector2 currentPosition = new Vector2(newPosition.X, newPosition.Y);

            for (int row = 0; row < newNumberOfRows; ++row)
            {
                baseTable[0][row].X = (int)currentPosition.X;
                baseTable[0][row].Y = (int)currentPosition.Y;
                currentPosition.Y += positionShift.Y;
            }
            lastRow[0].X = (int)currentPosition.X;
            lastRow[0].Y = (int)currentPosition.Y;

            currentPosition.X += shiftFirstCol.X;
            currentPosition.Y = newPosition.Y;

            for (int col = 1; col <= Game1.NumberOfPlayers; ++col)
            {
                for (int row = 0; row < newNumberOfRows; ++row)
                {
                    baseTable[col][row].X = (int)currentPosition.X;
                    baseTable[col][row].Y = (int)currentPosition.Y;
                    currentPosition.Y += positionShift.Y;
                }

                lastRow[col].X = (int)currentPosition.X;
                lastRow[col].Y = (int)currentPosition.Y;

                currentPosition.X += positionShift.X;
                currentPosition.Y = newPosition.Y;
            }

            position = newPosition;
            numberOfRows = newNumberOfRows;
        }

        public void Update(GameTime gameTime)
        {
            if (!IsActive)
                return;

            if (currentNumberOfPlayers != Game1.NumberOfPlayers)
            {
                if (currentNumberOfPlayers < Game1.NumberOfPlayers)
                    AddNewPlayer();
                else
                    DeleteLastPlayer();
                currentNumberOfPlayers = Game1.NumberOfPlayers;
            }
            
            foreach (List<TextBox.TextBox> col in baseTable)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    col[row].Update(gameTime);
                }
            }
            if (CurrentField.IsChanged)
            {
                lastRow[GameLogic.GetCurrentPlayer].IntValue = lastRow[GameLogic.GetCurrentPlayer].IntValue 
                            + (Game1.CurrentPointsGameMode == Game1.PointsGameModes.None ? 1 : -1)*currentTextField.LastValue;
                lastRow[GameLogic.GetCurrentPlayer].Text = lastRow[GameLogic.GetCurrentPlayer].IntValue.ToString();
                CurrentField.IsChanged = false;
            }
            for (int i = 0; i <= Game1.NumberOfPlayers; ++i)
                lastRow[i].Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            foreach (List<TextBox.TextBox> col in baseTable)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    col[row].Draw(spriteBatch, gameTime);
                }
            }           
            for (int i = 0; i <= Game1.NumberOfPlayers; ++i)
                lastRow[i].Draw(spriteBatch, gameTime);

            spriteBatch.End();
        }

        public void SetNextField()
        {
            if (currentTextField != null)
                currentTextField.IsActive = false;
            if (Darts.GameLogic.GetCurrentMove >= numberOfRows)
            {
                if (Game1.CurrentGameType == Game1.GameTypes.BigRound)
                    AddSomeRows(1, 25);
                else
                    AddSomeRows();
            }

            previousTextField = currentTextField;
            currentTextField = baseTable[Darts.GameLogic.GetCurrentPlayer][Darts.GameLogic.GetCurrentMove];
            currentTextField.IsActive = true;
        }

        public Darts.TextBox.TextBox CurrentField
        {
            get
            {
                return currentTextField;
            }
            set
            {
                if (currentTextField != null)
                    currentTextField.IsActive = false;
                currentTextField = value;
                currentTextField.IsActive = true;
            }
        }

        public void SubLastMove()
        {
            TextBox.TextBox field = CurrentField;

            if (Game1.CurrentGameType == Game1.GameTypes.CricketWithPoints || Game1.CurrentGameType == Game1.GameTypes.Cricket)
            {
                if (CurrentField.ValuesNumber == 3)
                {
                    lastRow[GameLogic.GetLastPlayer].IntValue += (Game1.CurrentPointsGameMode == Game1.PointsGameModes.None ? -1 : 1) * field.IntValue;
                    lastRow[GameLogic.GetLastPlayer].Text = lastRow[GameLogic.GetLastPlayer].IntValue.ToString();
                    GameLogic.SetPreviousMove();
                }
                else
                {
                    lastRow[GameLogic.GetCurrentPlayer].IntValue += (Game1.CurrentPointsGameMode == Game1.PointsGameModes.None ? -1 : 1) * field.IntValue;
                    lastRow[GameLogic.GetCurrentPlayer].Text = lastRow[GameLogic.GetCurrentPlayer].IntValue.ToString();
                    GameLogic.ResetShots();
                }

                field.ResetCrosses();
                field.IntValue = 0;
                field.ValuesNumber = 0;
                field.Text = defaultString;
                return;
            }

            if (CurrentField.Text == defaultString && previousTextField != null)
            {
                field = previousTextField;
                lastRow[GameLogic.GetLastPlayer].IntValue += (Game1.CurrentPointsGameMode == Game1.PointsGameModes.None ? -1 : 1) * field.IntValue;
                lastRow[GameLogic.GetLastPlayer].Text = lastRow[GameLogic.GetLastPlayer].IntValue.ToString();
                currentTextField.IsActive = false;
                currentTextField = GetLastField;
                currentTextField.IsActive = true;
                GameLogic.SetPreviousMove();
            }
            else
            {
                lastRow[GameLogic.GetCurrentPlayer].IntValue += (Game1.CurrentPointsGameMode == Game1.PointsGameModes.None ? -1 : 1) * field.IntValue;
                lastRow[GameLogic.GetCurrentPlayer].Text = lastRow[GameLogic.GetCurrentPlayer].IntValue.ToString();
                GameLogic.ResetShots();
            }


            field.IntValue = 0;
            field.ValuesNumber = 0;
            field.Text = defaultString;
        }

        public void InitCols()
        {
            for (int player = 1; player <= Game1.NumberOfPlayers; ++player)
            {
                InitCol(baseTable[player], 0);
            }
        }
        public void InitLastRow(int startScore)
        {
            for (int i = 1; i <= Game1.NumberOfPlayers; ++i)
            {
                lastRow[i].Text = startScore.ToString();
                lastRow[i].IntValue = startScore;
            }
            initScore = startScore;
        }

        public int GetPlayerScore(int player)
        {
            return lastRow[player].IntValue;
        }

        public void PrepareForGame()
        {
            //if this table was prepared for cricket
            if (numberOfRows == 8)
            {
                currentMoveField.IsActive = true;
                //remark first table
                for (int row = 1; row < numberOfRows; ++row)
                {
                    baseTable[0][row].Text = row.ToString();
                }
                //add some rows
                int newNumberOfRows = 21;
                //if (Game1.CurrentGameType == Game1.GameTypes.CollectPoints)
                //{
                //    newNumberOfRows = 11;
                //}
                for (int row = 8; row < newNumberOfRows; ++row)
                {
                    AddSomeRows(1, row);
                }
            }
            if (Game1.CurrentGameType == Game1.GameTypes.CollectPoints || GameLogic.NumberOfMoves == 10 && Game1.IsPointsCurrrentGameType)
            {
                for (int row = 11; row < 19; ++row)
                {
                    DeleteRowAt(11);
                }
                numberOfRows = 10; //10 for points & 1 for names
                //rebuild all table + add one row
                AddSomeRows(1, 10);
            }
            if (GameLogic.NumberOfMoves == 30 && Game1.IsPointsCurrrentGameType)
            {
                for (int i = 21; i <= 30; ++i)
                {
                    AddSomeRows(1, i);
                }
            }
            if (Game1.CurrentGameType == Game1.GameTypes.BigRound)
            {
                AddSomeRows(1, 25);
            }
        }

        public void PrepareForCricket()
        {
            if (IsPreparedForCricket)
                return;

            currentMoveField.IsActive = false;
            //delete unnesessary fields
            for (int row = 1; row < 15; ++row)
            {
                DeleteRowAt(1);
            }
            //rebuild all table + add one row
            numberOfRows = 7; //7 for points & 1 for names
            AddSomeRows(1, 25);
            //change some settings for fields
            for (int col = 1; col <= Game1.NumberOfPlayers; ++col)
            {
                for (int row = 1; row <= 7; ++row)
                {
                    baseTable[col][row].Text = "---";
                }
            }

            currentTextField = baseTable[1][1];
            currentTextField.IsActive = true;
            defaultString = "---";
            IsPreparedForCricket = true;
        }
        void DeleteRowAt(int _index)
        {
            for (int col = 0; col <= Game1.NumberOfPlayers; ++col)
            {
                baseTable[col].RemoveAt(_index);
            }
        }


        public TextBox.TextBox GetFieldByCost(int _cost)
        {
            if (IsPreparedForCricket)
            {
                if (_cost <= 20)
                    return baseTable[GameLogic.GetCurrentPlayer][_cost - 15 + 1];
                else
                    return baseTable[GameLogic.GetCurrentPlayer][7];
            }
            return null;
        }
        bool IsPreparedForCricket
        {
            get;
            set;
        }

        bool IsActive
        {
            get;
            set;
        }

        public TextBox.TextBox GetLastField
        {
            get
            {
                if (previousTextField != null)
                    return previousTextField;
                else
                    return currentTextField;
            }
        }
        public string GetPlayerName(int player)
        {
            return baseTable[player][0].Text;
        }
        public void SetInActiveNames()
        {
            for (int player = 1; player <= Game1.NumberOfPlayers; ++player)
            {
                baseTable[player][0].IsReadOnly = true;
            }
        }

        public void HighLightCurrentMove()
        {
            if (IsPreparedForCricket)
                return;
            if (currentMoveField != null)
            {
                currentMoveField.IsActive = false;
            }
            currentMoveField = baseTable[0][GameLogic.GetCurrentMove];
            currentMoveField.IsActive = true;
        }
        public void HighLightCurrentPlayer()
        {
            if (currentPlayerField != null)
            {
                currentPlayerField.IsActive = false;
            }
            currentPlayerField = baseTable[GameLogic.GetCurrentPlayer][0];
            currentPlayerField.IsActive = true;
        }
    }
}
