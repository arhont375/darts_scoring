﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Darts.Text
{
    class StatsTable
    {
        int numberOfRows;
        /// <summary>
        /// Cols for players + hits + slips + scores + wins
        /// </summary>
        List<List<TextBox.TextBox>> baseTable;

        List<int> scores;

        readonly int NUMBER_OF_COLS = 5;

        SpriteFont font;
        Texture2D textureTextField;
        Texture2D textureCarret;

        Vector2 positionCenter;
        Vector2 position;
        Vector2 positionShift;
        Vector2 shiftFirstCol;

        Table tablePoints;

        int currentNumberOfPlayers;
        int textFieldLength = 100;
        int constTextFieldLength = 105;
        float scope = 1;
        string defaultString = "0";

        public StatsTable(Vector2 _centerPosition, Texture2D _textureTextFeild, Texture2D _textureCarret, SpriteFont _font, float _scope, Table _tablePoints)
        {
            positionCenter = _centerPosition;
            textureCarret = _textureCarret;
            textureTextField = _textureTextFeild;
            font = _font;
            tablePoints = _tablePoints;

            Initialize();
        }

        void InitCol(List<TextBox.TextBox> col, int _value = 0)
        {
            for (int row = 1; row < numberOfRows; ++row)
            {
                col[row].Text = defaultString;
                col[row].IsReadOnly = true;
            }
        }

        public void Initialize()
        {
            baseTable = new List<List<TextBox.TextBox>>(NUMBER_OF_COLS);

            currentNumberOfPlayers = Game1.NumberOfPlayers;

            scores = new List<int>(Game1.MAX_PLAYERS + 1);
            scores.Add(new int());
            scores.Add(new int());
            scores[1] = 0;

            numberOfRows = currentNumberOfPlayers + 1;
            position = new Vector2(positionCenter.X - (textFieldLength * (NUMBER_OF_COLS - 1) + constTextFieldLength) * scope / 2,
                                    positionCenter.Y - textureTextField.Height / 2 * scope * numberOfRows / 2);

            Vector2 currentPosition;
            currentPosition = position;


            positionShift = new Vector2(textFieldLength * scope, textureTextField.Height * scope / 2);
            shiftFirstCol = new Vector2(constTextFieldLength * scope, textureTextField.Height * scope / 2);

            for (int col = 0; col < NUMBER_OF_COLS; ++col)
            {
                baseTable.Add(new List<TextBox.TextBox>(numberOfRows));
            }
            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[0].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
                baseTable[0][row].IsReadOnly = true;
                baseTable[0][row].Width = constTextFieldLength;
                baseTable[0][row].X = (int)currentPosition.X;
                baseTable[0][row].Y = (int)currentPosition.Y;
                baseTable[0][row].Text = tablePoints.GetPlayerName(row);
                currentPosition.Y += positionShift.Y;
            }
            baseTable[0][0].Text = "Имя";

            currentPosition.X += shiftFirstCol.X;
            currentPosition.Y = position.Y;
            for (int col = 1; col < NUMBER_OF_COLS; ++col)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    baseTable[col].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
                    baseTable[col][row].Width = textFieldLength;
                    baseTable[col][row].X = (int)currentPosition.X;
                    baseTable[col][row].Y = (int)currentPosition.Y;
                    currentPosition.Y += positionShift.Y;
                }

                currentPosition.X += positionShift.X;
                currentPosition.Y = position.Y;
            }
            baseTable[1][0].Text = "Попадания";
            baseTable[2][0].Text = "Промахи";
            baseTable[3][0].Text = "Очки";
            baseTable[4][0].Text = "Побед";

            for (int col = 1; col < NUMBER_OF_COLS; ++col)
            {
                InitCol(baseTable[col]);
            }
        }

        void AddNewPlayer()
        {
            int newNumberOfRows = numberOfRows + 1;
            position = new Vector2(positionCenter.X - (textFieldLength * (NUMBER_OF_COLS - 1) + constTextFieldLength) * scope / 2,
                                    positionCenter.Y - textureTextField.Height / 2 * scope * newNumberOfRows / 2);

            Vector2 currentPosition;
            currentPosition = position;

            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[0][row].X = (int)currentPosition.X;
                baseTable[0][row].Y = (int)currentPosition.Y;
                currentPosition.Y += positionShift.Y;
            }
            baseTable[0].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
            baseTable[0][numberOfRows].IsReadOnly = true;
            baseTable[0][numberOfRows].Width = constTextFieldLength;
            baseTable[0][numberOfRows].X = (int)currentPosition.X;
            baseTable[0][numberOfRows].Y = (int)currentPosition.Y;
            baseTable[0][numberOfRows].Text = tablePoints.GetPlayerName(numberOfRows);

            currentPosition.X += shiftFirstCol.X;
            currentPosition.Y = position.Y;

            for (int col = 1; col < NUMBER_OF_COLS; ++col)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    baseTable[col][row].X = (int)currentPosition.X;
                    baseTable[col][row].Y = (int)currentPosition.Y;
                    currentPosition.Y += positionShift.Y;
                }
                baseTable[col].Add(new TextBox.TextBox(textureTextField, textureCarret, font, Game1.GetkeyboardDispatcher));
                baseTable[col][numberOfRows].Width = textFieldLength;
                baseTable[col][numberOfRows].IsReadOnly = true;
                baseTable[col][numberOfRows].Text = defaultString;
                baseTable[col][numberOfRows].X = (int)currentPosition.X;
                baseTable[col][numberOfRows].Y = (int)currentPosition.Y;

                currentPosition.X += positionShift.X;
                currentPosition.Y = position.Y;
            }

            scores.Add(new int());
            scores[numberOfRows] = 0;
            numberOfRows = newNumberOfRows;
        }

        void DeleteLastPlayer()
        {
            int newNumberOfRows = numberOfRows - 1;
            position = new Vector2(positionCenter.X - (textFieldLength * (NUMBER_OF_COLS - 1) + constTextFieldLength) * scope / 2,
                                    positionCenter.Y - textureTextField.Height / 2 * scope * newNumberOfRows / 2);


            for (int col = 0; col < NUMBER_OF_COLS; ++col)
            {
                baseTable[col].RemoveAt(newNumberOfRows);
            }

            Vector2 currentPosition;
            currentPosition = position;

            for (int row = 0; row < numberOfRows; ++row)
            {
                baseTable[0][row].X = (int)currentPosition.X;
                baseTable[0][row].Y = (int)currentPosition.Y;
                currentPosition.Y += positionShift.Y;
            }

            currentPosition.X += shiftFirstCol.X;
            currentPosition.Y = position.Y;
            
            for (int col = 1; col < NUMBER_OF_COLS; ++col)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    baseTable[col][row].X = (int)currentPosition.X;
                    baseTable[col][row].Y = (int)currentPosition.Y;
                    currentPosition.Y += positionShift.Y;
                }

                currentPosition.X += positionShift.X;
                currentPosition.Y = position.Y;
            }

            numberOfRows = newNumberOfRows;
            scores.Remove(newNumberOfRows);
        }

        public void RefreshStats()
        {
            if (currentNumberOfPlayers != Game1.NumberOfPlayers)
            {
                while (currentNumberOfPlayers < Game1.NumberOfPlayers)
                {
                    AddNewPlayer();
                    ++currentNumberOfPlayers;
                }
            }

            if (GameLogic.GetCurrentPlayer == 0)
            {
                GameLogic.SetPlayerWinner(GetPlayerNumberWithMaxScores());
            }

            for (int row = 1; row < numberOfRows; ++row)
            {
                baseTable[0][row].Text = tablePoints.GetPlayerName(row);
                baseTable[1][row].Text = GameLogic.GetPlayerHits(row).ToString();
                baseTable[2][row].Text = GameLogic.GetPlayerSlips(row).ToString();

                if (GameLogic.IsLegOver)
                {
                    scores[row] += tablePoints.GetPlayerScore(row);
                    baseTable[3][row].Text = scores[row].ToString();
                }
                else
                {
                    baseTable[3][row].Text = (scores[row] + tablePoints.GetPlayerScore(row)).ToString();
                }


                baseTable[4][row].Text = GameLogic.GetPlayerWins(row).ToString();
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (List<TextBox.TextBox> col in baseTable)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    col[row].Update(gameTime);
                }
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (List<TextBox.TextBox> col in baseTable)
            {
                for (int row = 0; row < numberOfRows; ++row)
                {
                    col[row].Draw(spriteBatch, gameTime);
                }
            }
        }

        public int GetPlayerNumberWithMaxScores()
        {
            int index = 1;
            for (int i = 2; i < numberOfRows; ++i)
            {
                if (scores[index] < scores[i])
                    index = i;
            }

            return index;
        }
    }
}
