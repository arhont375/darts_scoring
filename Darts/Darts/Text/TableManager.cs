using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Darts.Text
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class TableManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;

        Texture2D textureTextField;
        Texture2D textureCarret;

        //for stats
        Texture2D blackFiltr;
        Texture2D windowBase;

        Vector2 statsWindowPosition;

        ActionButton buttonNext, buttonExit;
        Texture2D textureNext, textureExit;
        //--
        SpriteFont tableFont;


        static Table table;
        static StatsTable statsTable;

        public TableManager(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {


            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);


            tableFont = Game.Content.Load<SpriteFont>(@"fonts/normalTableFont");
            textureTextField = Game.Content.Load<Texture2D>(@"images/text_box/text_box_3");
            textureCarret = Game.Content.Load<Texture2D>(@"images/text_box/carret");


            table = new Table(new Vector2(Game.Window.ClientBounds.Width / 9 * 7 - 10, Game.Window.ClientBounds.Height / 2), textureTextField, textureCarret, tableFont, 1);

            //for stats

            blackFiltr = Game.Content.Load<Texture2D>(@"images/backgrounds/filtr");
            windowBase = Game.Content.Load<Texture2D>(@"images/backgrounds/base_window");

            textureExit = Game.Content.Load<Texture2D>(@"images/buttons/exit_ingame");
            textureNext = Game.Content.Load<Texture2D>(@"images/buttons/next_ingame");

            statsWindowPosition = new Vector2(Game.Window.ClientBounds.Width / 2 - windowBase.Width / 2, Game.Window.ClientBounds.Height / 2 - windowBase.Height / 2);
            statsTable = new StatsTable(new Vector2(Game.Window.ClientBounds.Width / 2, Game.Window.ClientBounds.Height / 2), textureTextField, textureCarret, tableFont, 1, table); 

            buttonExit = new ActionButton(textureExit, textureExit
                                , new Vector2(Game.Window.ClientBounds.Width / 2 + windowBase.Width / 2 - textureExit.Width / 2, Game.Window.ClientBounds.Height / 2 + windowBase.Height / 2)
                                , ButtonManager.ChangeGameState, Game1.GameStates.Start);
            buttonNext = new ActionButton(textureNext, textureNext
                                , new Vector2(Game.Window.ClientBounds.Width / 2 - windowBase.Width / 2 + textureExit.Width / 2, Game.Window.ClientBounds.Height / 2 + windowBase.Height / 2)
                                , ButtonManager.ChangeGameState, Game1.GameStates.InGame);
            //--
            
            base.LoadContent();
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            //if (!Game.IsActive)
            //    base.Update(gameTime);

            if (Game1.CurrentGameState == Game1.GameStates.InGameStats || Game1.CurrentGameState == Game1.GameStates.GameOver)
            {
                statsTable.Update(gameTime);
                if (Game1.CurrentGameState != Game1.GameStates.GameOver)
                    buttonNext.Update(gameTime);
                buttonExit.Update(gameTime);
            }

            if (Game1.CurrentGameState != Game1.GameStates.InGame && Game1.CurrentGameState != Game1.GameStates.InGameStart)
            {
                base.Draw(gameTime);
                return;
            }

            table.Update(gameTime);

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            //if (!Game.IsActive)
            //    base.Draw(gameTime);

            if (Game1.CurrentGameState != Game1.GameStates.InGame && Game1.CurrentGameState != Game1.GameStates.InGameStart 
                            && Game1.GameStates.InGameStats != Game1.CurrentGameState && Game1.CurrentGameState != Game1.GameStates.GameOver)
            {
                base.Draw(gameTime);
                return;
            }

            table.Draw(gameTime, spriteBatch);

            //TODO : �������� ���
            //���� ������� ���������, ��� ��� ����� ������ ����� ������� ����������� ������ � ��������� ��
            spriteBatch.Begin();
            if (Game1.CurrentGameState == Game1.GameStates.InGameStats || Game1.CurrentGameState == Game1.GameStates.GameOver)
            {

                spriteBatch.Draw(blackFiltr, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 0);
                spriteBatch.Draw(windowBase, statsWindowPosition, Color.White);

                statsTable.Draw(gameTime, spriteBatch);
                if (Game1.CurrentGameState != Game1.GameStates.GameOver)
                    buttonNext.Draw(gameTime, spriteBatch);

                buttonExit.Draw(gameTime, spriteBatch);

                if (GameLogic.IsLegOver)
                {
                    spriteBatch.DrawString(tableFont, "����� " + (GameLogic.GetCurrentLeg - 1).ToString() + " ����", statsWindowPosition + new Vector2(windowBase.Width / 3, 0), Color.White);
                    if (GameLogic.GetCurrentWinner != 0)
                        spriteBatch.DrawString(tableFont, "������� " + table.GetPlayerName(GameLogic.GetCurrentWinner), statsWindowPosition + new Vector2(windowBase.Width / 4, 20), Color.White);
                    else
                        spriteBatch.DrawString(tableFont, "������� " + table.GetPlayerName(statsTable.GetPlayerNumberWithMaxScores()), statsWindowPosition + new Vector2(windowBase.Width / 4, 20), Color.White);
                }
                else
                {
                    spriteBatch.DrawString(tableFont, "��� " + GameLogic.GetCurrentLeg.ToString() + " ���", statsWindowPosition + new Vector2(windowBase.Width / 3, 0), Color.White);
                    if (GameLogic.GetCurrentWinner == 0)
                    {
                        spriteBatch.DrawString(tableFont, "��� ��� ����������", statsWindowPosition + new Vector2(windowBase.Width / 4, 20), Color.White);
                    }
                    else
                    {
                        spriteBatch.DrawString(tableFont, "������� " + table.GetPlayerName(statsTable.GetPlayerNumberWithMaxScores()), statsWindowPosition + new Vector2(windowBase.Width / 4, 20), Color.White);
                    }
                }
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
        static public void SetInitScore(int startScore)
        {
            InitScore = startScore;
        }
        static public void SetCurrentField(Darts.TextBox.TextBox currentField)
        {
            table.CurrentField = currentField;
        }
        static public Darts.TextBox.TextBox GetCurrentField()
        {
            return table.CurrentField;
        }
        static public Darts.TextBox.TextBox GetNextField()
        {
            table.SetNextField();
            return table.CurrentField;
        }
        static public int GetCurrentPlayerSum()
        {
            return table.GetPlayerScore(GameLogic.GetCurrentPlayer);
        }
        static public void InitAllTables()
        {
            table.Initialize();
            statsTable.Initialize();
        }
        static public void InitTable()
        {
            if (GameLogic.GetCurrentLeg == 1)
                if (IsCricket)
                    table.PrepareForCricket();
                else
                    table.PrepareForGame();
            table.SetInActiveNames();
            table.InitCols();
            table.InitLastRow(InitScore);
        }
        /// <summary>
        /// Substraction all points from current field
        /// </summary>
        static public void SubLastMove()
        {
            table.SubLastMove();
        }
        static int InitScore
        {
            get;
            set;
        }
        static public TextBox.TextBox GetFieldByCost(int cost)
        {
            return table.GetFieldByCost(cost);
        }
        static public bool IsCricket
        {
            get;
            set;
        }
        static public void ResetTable()
        {
            table.Initialize();
        }
        static public void RefreshStats()
        {
            statsTable.RefreshStats();
        }
        static public void HighLightCurrentMove()
        {
            table.HighLightCurrentMove();
        }
        static public void HighLightCurrentPlayer()
        {
            table.HighLightCurrentPlayer();
        }
    }
}
