using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Darts
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    /// 
    public class ButtonManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;

        //Buttons
        ActionButton buttonExit, buttonNewGame, buttonStart, buttonBack;
        ActionButton button501, button301, button101, buttonBigRound, buttonCollectPoints, buttonCricket, buttonCricketWithPoints;
        ActionButton buttonSSSE, buttonSSDE, buttonDSDE; //SimpleStartSimpleEnd, SimpleStartDoubleEnd, DoubleStartDoubleEnd
        ActionButton buttonTournament7Legs, buttonTournament3Legs, buttonTournament5Legs;
        ActionButton buttonAddPlayer, buttonDeletePlayer;
        ActionButton buttonNext, buttonBack2;
        ActionButton buttonEndGame, buttonCancelMove;
        ActionButton buttonFullScreenToggle, buttonCreator;
        ActionButton button10turns, button30turns;

        static ActionButton button20turns, button1Leg;

        //Texture for buttons
        Texture2D textureButtonStartGameNormal, textureButtonExitNormal, textureStart, textureBack;
        Texture2D textureButton501, textureButton301, textureButton101, textureButtonBigRound, textureButtonCollectPoints, textureButtonCricket, textureButtonCricketWithPoints;
        Texture2D textureButtonSSSE, textureButtonSSDE, textureButtonDSDE; //SimpleStartSimpleEnd, SimpleStartDoubleEnd, DoubleStartDoubleEnd
        Texture2D textureTournament7Legs, textureTournament3Legs, textureTournament5Legs, texture1Leg;
        Texture2D textureAddPlayer, textureDeletePlayer;
        Texture2D textureNext;
        Texture2D textureEndGame, textureRestart, textureCancelMove;
        Texture2D textureFullScreenToggle, textureCreatore;
        Texture2D texture10turns, texture20turns, texture30turns;

        SpriteFont fontButton;

        static event ButtonDelegate UnhighlighteGameTypes;
        static event ButtonDelegate UnhighlghitePointsTypes;
        static event ButtonDelegate UnhighlighteLegs;
        static event ButtonDelegate UnhighlighteMoves;

        public ButtonManager(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);

            //Content for buttons
            fontButton = Game.Content.Load<SpriteFont>(@"fonts/normalTableFont");

            textureButtonExitNormal = Game.Content.Load<Texture2D>(@"images\buttons\exit_normal");
            textureButtonStartGameNormal = Game.Content.Load<Texture2D>(@"images\buttons\startgame_normal");
            textureStart = Game.Content.Load<Texture2D>(@"images\buttons\start");
            textureBack = Game.Content.Load<Texture2D>(@"images\buttons\back");

            textureButton101 = Game.Content.Load<Texture2D>(@"images\buttons\game_types\101_normal");
            textureButton301 = Game.Content.Load<Texture2D>(@"images\buttons\game_types\301_normal");
            textureButton501 = Game.Content.Load<Texture2D>(@"images\buttons\game_types\501_normal");
            textureButtonBigRound = Game.Content.Load<Texture2D>(@"images\buttons\game_types\big_round_normal");
            textureButtonCricket = Game.Content.Load<Texture2D>(@"images\buttons\game_types\cricket_normal");
            textureButtonCollectPoints = Game.Content.Load<Texture2D>(@"images\buttons\game_types\collect_points_normal");
            textureButtonCricketWithPoints = Game.Content.Load<Texture2D>(@"images\buttons\game_types\cricket_with_points_normal");

            textureButtonDSDE = Game.Content.Load<Texture2D>(@"images\buttons\points_game_modes\double_start_double_end_normal");
            textureButtonSSDE = Game.Content.Load<Texture2D>(@"images\buttons\points_game_modes\simple_start_double_end_normal");
            textureButtonSSSE = Game.Content.Load<Texture2D>(@"images\buttons\points_game_modes\simple_start_simple_end_normal");

            textureTournament3Legs = Game.Content.Load<Texture2D>(@"images\buttons\game_modes\3_lags");
            textureTournament5Legs = Game.Content.Load<Texture2D>(@"images\buttons\game_modes\5_lags");
            textureTournament7Legs = Game.Content.Load<Texture2D>(@"images\buttons\game_modes\7_lags");
            texture1Leg = Game.Content.Load<Texture2D>(@"images\buttons\game_modes\1_lags");

            textureAddPlayer = Game.Content.Load<Texture2D>(@"images\buttons\add_player");
            textureDeletePlayer = Game.Content.Load<Texture2D>(@"images\buttons\delete_player");

            textureNext = Game.Content.Load<Texture2D>(@"images\buttons\next_button");

            textureRestart = Game.Content.Load<Texture2D>(@"images\buttons\restart_game");
            textureEndGame = Game.Content.Load<Texture2D>(@"images\buttons\end_game");
            textureCancelMove = Game.Content.Load<Texture2D>(@"images\buttons\cancel_move");

            textureFullScreenToggle = Game.Content.Load<Texture2D>(@"images\buttons\fullscreen_toggle");
            textureCreatore = Game.Content.Load<Texture2D>(@"images\buttons\creator");

            texture10turns = Game.Content.Load<Texture2D>(@"images\buttons\10_moves");
            texture20turns = Game.Content.Load<Texture2D>(@"images\buttons\20_moves");
            texture30turns = Game.Content.Load<Texture2D>(@"images\buttons\30_moves");


            Vector2 startPosition = new Vector2(Game.Window.ClientBounds.Width / 2, Game.Window.ClientBounds.Height / 2);
            Vector2 distanceButton = new Vector2(0, textureButtonExitNormal.Height + 30);

            //Create some buttons for startscreen
            buttonNewGame = new ActionButton(textureButtonStartGameNormal, textureButtonStartGameNormal, startPosition, ChangeGameState, Game1.GameStates.NewGameMenu);
            buttonFullScreenToggle = new ActionButton(textureFullScreenToggle, textureFullScreenToggle, startPosition + distanceButton, Game1.FullScreenToggle, null);
            buttonCreator = new ActionButton(textureCreatore, textureCreatore, startPosition + 2 * distanceButton, ChangeGameState, Game1.GameStates.About);
            buttonExit = new ActionButton(textureButtonExitNormal, textureButtonExitNormal, startPosition + 3 * distanceButton, ChangeGameState, Game1.GameStates.Exit);


            //create some buttons for newgame sreen
            startPosition = new Vector2(Game.Window.ClientBounds.Width / 5 * 2 - textureButton101.Width, Game.Window.ClientBounds.Height / 4);
            distanceButton = new Vector2(Game.Window.ClientBounds.Width / 5, 0);

            button101 = new ActionButton(textureButton101, textureButton101, startPosition, ChangeGameType, Game1.GameTypes.Points101);
            button301 = new ActionButton(textureButton301, textureButton301, startPosition + distanceButton, ChangeGameType, Game1.GameTypes.Points301);
            button501 = new ActionButton(textureButton501, textureButton501, startPosition + 2 * distanceButton, ChangeGameType, Game1.GameTypes.Points501);
            //---
            distanceButton.X -= 20;
            startPosition += new Vector2(-60, textureButtonExitNormal.Height + 30);

            buttonBigRound = new ActionButton(textureButtonBigRound, textureButtonBigRound, startPosition, ChangeGameType, Game1.GameTypes.BigRound);
            buttonCricket = new ActionButton(textureButtonCricket, textureButtonCricket, startPosition + distanceButton,
                                                        ChangeGameType, Game1.GameTypes.Cricket);
            buttonCricketWithPoints = new ActionButton(textureButtonCricketWithPoints, textureButtonCricketWithPoints, startPosition + 2 * distanceButton,
                                                        ChangeGameType, Game1.GameTypes.CricketWithPoints);
            buttonCollectPoints = new ActionButton(textureButtonCollectPoints, textureButtonCollectPoints, startPosition + 3 * distanceButton,
                                                        ChangeGameType, Game1.GameTypes.CollectPoints);

            UnhighlighteGameTypes = button101.GetEventHandler + button301.GetEventHandler + button501.GetEventHandler;
            UnhighlighteGameTypes += buttonBigRound.GetEventHandler + buttonCricket.GetEventHandler + buttonCricketWithPoints.GetEventHandler;
            UnhighlighteGameTypes += buttonCollectPoints.GetEventHandler;

            button101.AddEventHandler = UnhighlighteGameTypes - button101.GetEventHandler;
            button301.AddEventHandler = UnhighlighteGameTypes - button301.GetEventHandler;
            button501.AddEventHandler = UnhighlighteGameTypes - button501.GetEventHandler;
            buttonBigRound.AddEventHandler = UnhighlighteGameTypes - buttonBigRound.GetEventHandler;
            buttonCricket.AddEventHandler = UnhighlighteGameTypes - buttonCricket.GetEventHandler;
            buttonCricketWithPoints.AddEventHandler = UnhighlighteGameTypes - buttonCricketWithPoints.GetEventHandler;
            buttonCollectPoints.AddEventHandler = UnhighlighteGameTypes - buttonCollectPoints.GetEventHandler;

            //---
            startPosition = new Vector2(Game.Window.ClientBounds.Width / 3 - textureButtonSSSE.Width / 2, Game.Window.ClientBounds.Height / 7 * 3 + 2*textureButtonSSSE.Height);
            distanceButton.X = textureButtonSSSE.Width + 20;
            buttonDSDE = new ActionButton(textureButtonDSDE, textureButtonDSDE, startPosition, ChangePointsGameMode, Game1.PointsGameModes.DoubleStartDoubleEnd);
            buttonSSSE = new ActionButton(textureButtonSSSE, textureButtonSSSE, startPosition + distanceButton, ChangePointsGameMode, Game1.PointsGameModes.SimpleStartSimpleEnd);
            buttonSSDE = new ActionButton(textureButtonSSDE, textureButtonSSDE, startPosition + 2 * distanceButton, ChangePointsGameMode, Game1.PointsGameModes.SimpleStartDoubleEnd);

            UnhighlghitePointsTypes = buttonDSDE.GetEventHandler + buttonSSSE.GetEventHandler + buttonSSDE.GetEventHandler;
            buttonDSDE.AddEventHandler = UnhighlghitePointsTypes - buttonDSDE.GetEventHandler;
            buttonSSSE.AddEventHandler = UnhighlghitePointsTypes - buttonSSSE.GetEventHandler;
            buttonSSDE.AddEventHandler = UnhighlghitePointsTypes - buttonSSDE.GetEventHandler;
            //---
            startPosition += new Vector2(0, texture1Leg.Height * 3);
            distanceButton.X = texture1Leg.Width + 20;
            button1Leg = new ActionButton(texture1Leg, texture1Leg, startPosition, ChangeGameMode, Game1.GameModes.Simple);
            buttonTournament3Legs = new ActionButton(textureTournament3Legs, textureTournament3Legs, startPosition + distanceButton, ChangeGameMode, Game1.GameModes.Tournament3Legs);
            buttonTournament5Legs = new ActionButton(textureTournament5Legs, textureTournament5Legs, startPosition + 2 * distanceButton, ChangeGameMode, Game1.GameModes.Tournament5Legs);
            buttonTournament7Legs = new ActionButton(textureTournament7Legs, textureTournament7Legs, startPosition + 3 * distanceButton, ChangeGameMode, Game1.GameModes.Tournament7Legs);

            button1Leg.IsHighlighted = true;

            UnhighlighteLegs = button1Leg.GetEventHandler + buttonTournament3Legs.GetEventHandler + buttonTournament5Legs.GetEventHandler + buttonTournament7Legs.GetEventHandler;

            button1Leg.AddEventHandler = UnhighlighteLegs - button1Leg.GetEventHandler;
            buttonTournament3Legs.AddEventHandler = UnhighlighteLegs - buttonTournament3Legs.GetEventHandler;
            buttonTournament5Legs.AddEventHandler = UnhighlighteLegs - buttonTournament5Legs.GetEventHandler;
            buttonTournament7Legs.AddEventHandler = UnhighlighteLegs - buttonTournament7Legs.GetEventHandler;
            //---
            buttonStart = new ActionButton(textureStart, textureStart, new Vector2(Game.Window.ClientBounds.Width / 10 * 9, Game.Window.ClientBounds.Height / 13 * 12),
                                                    ChangeGameState, Game1.GameStates.InGame);
            buttonBack = new ActionButton(textureBack, textureBack, new Vector2(Game.Window.ClientBounds.Width / 6, Game.Window.ClientBounds.Height / 7 * 6),
                                                    ChangeGameState, Game1.GameStates.Start);
            buttonNext = new ActionButton(textureNext, textureNext, new Vector2(Game.Window.ClientBounds.Width / 6 * 5, Game.Window.ClientBounds.Height / 7 * 6),
                                                    ChangeGameState, Game1.GameStates.InGameStart);
            buttonBack2 = new ActionButton(textureBack, textureBack, new Vector2(Game.Window.ClientBounds.Width / 10, Game.Window.ClientBounds.Height / 13 * 12),
                                                    ChangeGameState, Game1.GameStates.NewGameMenu);


            buttonAddPlayer = new ActionButton(textureAddPlayer, textureAddPlayer, new Vector2(Game.Window.ClientBounds.Width / 6 * 5, Game.Window.ClientBounds.Height / 7 * 6 - textureNext.Height),
                                                    ChangeNumberOfPlayers, 1);
            buttonDeletePlayer = new ActionButton(textureDeletePlayer, textureDeletePlayer, new Vector2(Game.Window.ClientBounds.Width / 6 * 5 - textureAddPlayer.Width, Game.Window.ClientBounds.Height / 7 * 6 - textureNext.Height),
                                                    ChangeNumberOfPlayers, -1);

            button10turns = new ActionButton(texture10turns, texture10turns, new Vector2(Game.Window.ClientBounds.Width / 6 * 5, Game.Window.ClientBounds.Height / 7 * 6),
                                                    ChangeNumberOfMoves, 10);
            button20turns = new ActionButton(texture20turns, texture20turns, new Vector2(Game.Window.ClientBounds.Width / 6 * 5 - textureAddPlayer.Width, Game.Window.ClientBounds.Height / 7 * 6),
                                                    ChangeNumberOfMoves, 20);
            button30turns = new ActionButton(texture30turns, texture30turns, new Vector2(Game.Window.ClientBounds.Width / 6 * 5 - 2 * textureAddPlayer.Width, Game.Window.ClientBounds.Height / 7 * 6),
                                                    ChangeNumberOfMoves, 30); 
            
            button20turns.IsHighlighted = true;

            UnhighlighteMoves = button10turns.GetEventHandler + button20turns.GetEventHandler + button30turns.GetEventHandler;
            button10turns.AddEventHandler = UnhighlighteMoves - button10turns.GetEventHandler;
            button20turns.AddEventHandler = UnhighlighteMoves - button20turns.GetEventHandler;
            button30turns.AddEventHandler = UnhighlighteMoves - button30turns.GetEventHandler;

            buttonEndGame = new ActionButton(textureEndGame, textureEndGame, new Vector2(Game.Window.ClientBounds.Width / 10 * 9, Game.Window.ClientBounds.Height / 13),
                                                    ChangeGameState, Game1.GameStates.InGameStats);
            buttonCancelMove = new ActionButton(textureCancelMove, textureCancelMove, new Vector2(Game.Window.ClientBounds.Width / 10 * 9, Game.Window.ClientBounds.Height / 13 * 12),
                                                    SubLastMove, 0);
            
            //buttonRestart = new ActionButton(textureEndGame, textureEndGame, new Vector2(),
            //                                        ChangeGameState, Game1.GameStates.InGameStats);

            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {


            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            switch (Game1.CurrentGameState)
            {
                case Game1.GameStates.Start:
                    {
                        buttonExit.Update(gameTime);
                        buttonCreator.Update(gameTime);
                        buttonFullScreenToggle.Update(gameTime);
                        buttonNewGame.Update(gameTime);

                        break;
                    }
                case Game1.GameStates.About:
                    {
                        buttonBack.Update(gameTime);
                        break;
                    }
                case Game1.GameStates.GameOver:
                    {
                        break;
                    }
                case Game1.GameStates.InGame:
                    {
                        buttonEndGame.Update(gameTime);
                        buttonCancelMove.Update(gameTime);
                        break;
                    }
                case Game1.GameStates.InGameStart:
                    {
                        if (Game1.NumberOfPlayers == Game1.MAX_PLAYERS)
                        {
                            buttonAddPlayer.IsActive = false;
                        }
                        else
                        {
                            buttonAddPlayer.IsActive = true;
                        }
                        if (Game1.NumberOfPlayers == Game1.MIN_PLAYERS)
                        {
                            buttonDeletePlayer.IsActive = false;
                        }
                        else
                        {
                            buttonDeletePlayer.IsActive = true;
                        }

                        buttonDeletePlayer.Update(gameTime);
                        buttonAddPlayer.Update(gameTime);
                        buttonStart.Update(gameTime);
                        buttonBack2.Update(gameTime);
                        if (Game1.IsPointsCurrrentGameType)
                        {
                            button10turns.Update(gameTime);
                            button20turns.Update(gameTime);
                            button30turns.Update(gameTime);
                        }

                        break;
                    }
                case Game1.GameStates.InGameStats:
                    {
                        break;
                    }
                case Game1.GameStates.Exit:
                    {
                        break;
                    }
                case Game1.GameStates.Options:
                    {
                        break;
                    }
                case Game1.GameStates.NewGameMenu:
                    {
                        if (Game1.CurrentGameType == Game1.GameTypes.Points101 || Game1.CurrentGameType == Game1.GameTypes.Points301 || Game1.CurrentGameType == Game1.GameTypes.Points501)
                        {
                            buttonSSDE.IsActive = buttonSSSE.IsActive = buttonDSDE.IsActive = true;
                        }
                        else
                        {
                            Game1.CurrentPointsGameMode = Game1.PointsGameModes.None;
                            UnhighlghitePointsTypes();
                            buttonSSDE.IsActive = buttonSSSE.IsActive = buttonDSDE.IsActive = false;
                        }

                                                                                                            
                        button101.Update(gameTime);
                        button301.Update(gameTime);
                        button501.Update(gameTime);
                        buttonCricket.Update(gameTime);
                        buttonCricketWithPoints.Update(gameTime);
                        buttonCollectPoints.Update(gameTime);
                        buttonBigRound.Update(gameTime);

                        buttonDSDE.Update(gameTime);
                        buttonSSDE.Update(gameTime);
                        buttonSSSE.Update(gameTime);

                        button1Leg.Update(gameTime);
                        buttonTournament3Legs.Update(gameTime);
                        buttonTournament5Legs.Update(gameTime);
                        buttonTournament7Legs.Update(gameTime);

                        buttonBack.Update(gameTime);
                        if (Game1.CurrentGameType == Game1.GameTypes.None || Game1.IsPointsCurrrentGameType && Game1.CurrentPointsGameMode == Game1.PointsGameModes.None)
                        {
                            buttonNext.IsActive = false;
                        }
                        else
                        {
                            buttonNext.IsActive = true;
                        }
                        buttonNext.Update(gameTime);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            base.Update(gameTime);
        }
        /// <summary>
        /// Allow th game component to draw her elements
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            switch (Game1.CurrentGameState)
            {
                case Game1.GameStates.Start:
                    {
                        spriteBatch.Begin();

                        buttonExit.Draw(gameTime, spriteBatch);
                        buttonNewGame.Draw(gameTime, spriteBatch);
                        buttonFullScreenToggle.Draw(gameTime, spriteBatch);
                        buttonCreator.Draw(gameTime, spriteBatch);

                        spriteBatch.End();
                        break;
                    }
                case Game1.GameStates.About:
                    {
                        spriteBatch.Begin();

                        buttonBack.Draw(gameTime, spriteBatch);

                        spriteBatch.End();

                        break;
                    }
                case Game1.GameStates.GameOver:
                    {
                        spriteBatch.Begin();

                        buttonCancelMove.Draw(gameTime, spriteBatch);
                        buttonEndGame.Draw(gameTime, spriteBatch);

                        spriteBatch.End();
                        break;
                    }
                case Game1.GameStates.InGame:
                    {
                        spriteBatch.Begin();

                        buttonCancelMove.Draw(gameTime, spriteBatch);
                        buttonEndGame.Draw(gameTime, spriteBatch);

                        spriteBatch.End();
                        break;
                    }
                case Game1.GameStates.InGameMenu:
                    {
                        break;
                    }
                case Game1.GameStates.InGameStart:
                    {
                        spriteBatch.Begin();

                        buttonAddPlayer.Draw(gameTime, spriteBatch);
                        buttonDeletePlayer.Draw(gameTime, spriteBatch);
                        buttonStart.Draw(gameTime, spriteBatch);
                        buttonBack2.Draw(gameTime, spriteBatch);
                        if (Game1.IsPointsCurrrentGameType)
                        {
                            button10turns.Draw(gameTime, spriteBatch);
                            button20turns.Draw(gameTime, spriteBatch);
                            button30turns.Draw(gameTime, spriteBatch);
                        }

                        spriteBatch.End();

                        break;
                    }
                case Game1.GameStates.InGameStats:
                    {
                        spriteBatch.Begin();

                        buttonCancelMove.Draw(gameTime, spriteBatch);
                        buttonEndGame.Draw(gameTime, spriteBatch);

                        spriteBatch.End();
                        break;
                    }
                case Game1.GameStates.Options:
                    {
                        break;
                    }
                case Game1.GameStates.NewGameMenu:
                    {
                        spriteBatch.Begin();
                        
                        button101.Draw(gameTime, spriteBatch);
                        button301.Draw(gameTime, spriteBatch);
                        button501.Draw(gameTime, spriteBatch);
                        buttonCricket.Draw(gameTime, spriteBatch);
                        buttonCricketWithPoints.Draw(gameTime, spriteBatch);
                        buttonCollectPoints.Draw(gameTime, spriteBatch);
                        buttonBigRound.Draw(gameTime, spriteBatch);

                        buttonDSDE.Draw(gameTime, spriteBatch);
                        buttonSSDE.Draw(gameTime, spriteBatch);
                        buttonSSSE.Draw(gameTime, spriteBatch);

                        button1Leg.Draw(gameTime, spriteBatch);
                        buttonTournament3Legs.Draw(gameTime, spriteBatch);
                        buttonTournament5Legs.Draw(gameTime, spriteBatch);
                        buttonTournament7Legs.Draw(gameTime, spriteBatch);

                        buttonBack.Draw(gameTime, spriteBatch);
                        buttonNext.Draw(gameTime, spriteBatch);

                        spriteBatch.End();
                        break;
                    }
                default:
                    {
                        break;
                    }
            } 

            base.Draw(gameTime);
        }

        static public void DeselectAllButtons()
        {
            UnhighlighteGameTypes();
            UnhighlghitePointsTypes();
            UnhighlighteLegs();
            UnhighlighteMoves();

            button1Leg.IsHighlighted = true;
            button20turns.IsHighlighted = true;
        }
        static public void ChangeNumberOfMoves(object _numberOfMove)
        {
            GameLogic.NumberOfMoves = (int)_numberOfMove;
        }
        static public void SubLastMove(object move)
        {
            Darts.GameLogic.ResetShots();
            Text.TableManager.SubLastMove();
        }
        static public void ChangeNumberOfPlayers(object change)
        {
            int chng = (int)change;
            if (chng < 0)
                Game1.NumberOfPlayers = Game1.NumberOfPlayers - 1;
            else
                Game1.NumberOfPlayers = Game1.NumberOfPlayers + 1;
        }
        static public void ChangeGameMode(object newGameMode)
        {
            Game1.CurrentGameMode = (Game1.GameModes)newGameMode;
        }
        static public void ChangePointsGameMode(object newGameMode)
        {
            Game1.CurrentPointsGameMode = (Game1.PointsGameModes)newGameMode;
        }
        static public void ChangeGameType(object newGameType)
        {
            Game1.CurrentGameType = (Game1.GameTypes)newGameType;
        }
        static public void ChangeGameState(object newGameState)
        {
            Game1.CurrentGameState = (Game1.GameStates)newGameState;
        }

    }
}
