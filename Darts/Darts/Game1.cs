using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Darts
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        static GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ButtonManager buttonManager;
        static Board.BoardManager boardManager;
        Text.TableManager tableManager;
        static GameLogic gameLogic;

        //Catch keyboard keys
        static EventInput.KeyboardDispatcher keyboardDispatcher;

        //Game states
        public enum GameStates { Start, InGameStart, InGame, GameOver, Options, InGameMenu, Exit, NewGameMenu, InGameStats, About };
        static GameStates currentGameState = GameStates.Start;
        //Game types
        public enum GameTypes { None, Points501, Points301, Points101, BigRound, CollectPoints, Cricket, CricketWithPoints };
        static GameTypes currentGameType = GameTypes.None;
        //Game modes
        public enum GameModes { Simple, Tournament7Legs, Tournament3Legs, Tournament5Legs };
        static GameModes currentGameMode = GameModes.Simple;
        //Game modes for Points***
        public enum PointsGameModes { None, SimpleStartSimpleEnd, SimpleStartDoubleEnd, DoubleStartDoubleEnd };
        static PointsGameModes currentPointsGameMode = PointsGameModes.None;

        static int numberOfPlayers = 1;
        public static readonly int MAX_PLAYERS = 4;
        public static readonly int MIN_PLAYERS = 1;

        static float globalScope = 1.0f / 2.0f;

        //Game Backgrounds
        Texture2D backgroundStart;
        Texture2D backGroundCreator;
        Texture2D blackFiltr;
        SpriteFont basicFont;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;

            graphics.IsFullScreen = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            this.IsMouseVisible = true;

            IsPointsCurrrentGameType = false;

            keyboardDispatcher = new EventInput.KeyboardDispatcher(this.Window);

            buttonManager = new ButtonManager(this);
            boardManager = new Board.BoardManager(this);
            tableManager = new Text.TableManager(this);
            gameLogic = new GameLogic(this);

            Components.Add(buttonManager);
            Components.Add(boardManager);
            Components.Add(tableManager);
            Components.Add(gameLogic);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            backgroundStart = Content.Load<Texture2D>(@"images/backgrounds/4_3_background_basic");
            backGroundCreator = Content.Load<Texture2D>(@"images/backgrounds/4_3_background_creator");
            basicFont = Content.Load<SpriteFont>(@"fonts/inGameFont");
            blackFiltr = Content.Load<Texture2D>(@"images/backgrounds/filtr");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (!this.IsActive)
                base.Update(gameTime);


            switch (currentGameState)
            {
                case GameStates.Start :
                    {
                        break;
                    }
                case GameStates.GameOver :
                    {
                        break;
                    }
                case GameStates.InGame :
                    {
                        break;
                    }
                case GameStates.Exit :
                    {
                        this.Exit();
                        break;
                    }
                default :
                    {
                        break;
                    }
            }

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (!this.IsActive)
                base.Draw(gameTime);

            spriteBatch.Begin();
            switch (currentGameState)
            {
                case GameStates.Start:
                    {
                        spriteBatch.Draw(backgroundStart, new Vector2(0, 0), Color.White);
                        break;
                    }
                case Game1.GameStates.About:
                    {
                        spriteBatch.Draw(backGroundCreator, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 1);
                        spriteBatch.DrawString(basicFont, "���������� ������� � ������� ������� �������� (2013-2014)", new Vector2(Window.ClientBounds.Width / 6, Window.ClientBounds.Height / 7), Color.White);
                        spriteBatch.DrawString(basicFont, "�� ������ ����� ��������� 231 ������", new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 7 + 30), Color.White);
                        spriteBatch.DrawString(basicFont, "��������� �����������", new Vector2(Window.ClientBounds.Width / 3 * 2, Window.ClientBounds.Height / 7 + 60), Color.White);
                        break;
                    }
                case GameStates.GameOver:
                    {
                        spriteBatch.Draw(backgroundStart, Vector2.Zero, null, Color.DarkSlateGray, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 1);
                        spriteBatch.Draw(blackFiltr, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 0);
                        break;
                    }
                case GameStates.InGame:
                    {
                        spriteBatch.Draw(backgroundStart, Vector2.Zero, null, Color.DarkSlateGray, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 1);
                        break;
                    }
                case GameStates.InGameStart:
                    {
                        spriteBatch.Draw(backgroundStart, Vector2.Zero, null, Color.DarkSlateGray, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 1);
                        spriteBatch.DrawString(basicFont, "������� �� ��� ������,", new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 7 ), Color.DarkRed);
                        spriteBatch.DrawString(basicFont, "�� ������ ������ ������ ���", new Vector2(Window.ClientBounds.Width / 20 * 9, Window.ClientBounds.Height / 16 * 3), Color.DarkRed);
                        break;
                    }
                case GameStates.InGameStats:
                    {
                        spriteBatch.Draw(backgroundStart, Vector2.Zero, null, Color.DarkSlateGray, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 1);
                        spriteBatch.Draw(blackFiltr, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 0);
                        break;
                    }
                case GameStates.Options:
                    {
                        break;
                    }
                case GameStates.NewGameMenu:
                    {
                        spriteBatch.Draw(backgroundStart, Vector2.Zero, null, Color.DarkSlateGray, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 1);
                        spriteBatch.DrawString(basicFont, "�������� ��� ����", new Vector2(Window.ClientBounds.Width / 3, Window.ClientBounds.Height / 6), Color.Red);
                        if (! IsPointsCurrrentGameType)
                        {
                            spriteBatch.DrawString(basicFont, "�������� ������ ����", new Vector2(Window.ClientBounds.Width / 3, Window.ClientBounds.Height / 7 * 3), Color.Gray);
                        }
                        else
                        {
                            spriteBatch.DrawString(basicFont, "�������� ������ ����", new Vector2(Window.ClientBounds.Width / 3, Window.ClientBounds.Height / 7 * 3), Color.Red);
                        }
                        spriteBatch.DrawString(basicFont, "�������� ���������� ���", new Vector2(Window.ClientBounds.Width / 7 * 2, Window.ClientBounds.Height / 23 * 14), Color.Red);                        
                        break;
                    }
                default:
                    {
                        GraphicsDevice.Clear(Color.Black);
                        break;
                    }
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        static public float GlobalScope
        {
            get
            {
                return globalScope;
            }
            set
            {
                globalScope = value;
            }
        }
        static public EventInput.KeyboardDispatcher GetkeyboardDispatcher
        {
            get
            {
                return keyboardDispatcher;
            }
        }
        static public int NumberOfPlayers
        {
            get
            {
                return numberOfPlayers;
            }
            set
            {
                if (value <= MAX_PLAYERS && Math.Abs(value - numberOfPlayers) == 1 && value >= MIN_PLAYERS)
                    numberOfPlayers = value;
            }
        }
        static public GameStates CurrentGameState
        {
            get
            {
                return currentGameState;
            }
            set
            {
                if (value == GameStates.InGameStats || value == GameStates.GameOver)
                {
                    Text.TableManager.RefreshStats();
                }

                if (value != currentGameState)
                {
                    if (currentGameState == GameStates.Start)
                    {
                        InitGame();
                    }
                    currentGameState = value;
                }
            }
        }
        static void InitGame()
        {
            NumberOfPlayers = 1;
            CurrentGameType = GameTypes.None;
            CurrentPointsGameMode = PointsGameModes.None;
            IsPointsCurrrentGameType = false;

            GameLogic.NumberOfMoves = 20;
            gameLogic.Initialize();
            GameLogic.IsInitialized = false;

            boardManager.Initialize();

            Text.TableManager.InitAllTables();
            ButtonManager.DeselectAllButtons();
        }
        static public GameTypes CurrentGameType
        {
            get
            {
                return currentGameType;
            }
            set
            {
                if (value != currentGameType)
                {
                    currentGameType = value;
                    if (currentGameType == GameTypes.Points101 || currentGameType == GameTypes.Points301 || currentGameType == GameTypes.Points501)
                    {
                        IsPointsCurrrentGameType = true;
                    }
                    else
                    {
                        IsPointsCurrrentGameType = false;
                    }
                }
            }
        }
        static public GameModes CurrentGameMode
        {
            get
            {
                return currentGameMode;
            }
            set
            {
                if (value != currentGameMode)
                {
                    currentGameMode = value;
                    switch (currentGameMode)
                    {
                        case GameModes.Tournament3Legs : 
                            {
                                GameLogic.MaxLegs = 3;
                                break;
                            }
                        case GameModes.Tournament5Legs :
                            {
                                GameLogic.MaxLegs = 5;
                                break;
                            }
                        case GameModes.Tournament7Legs :
                            {
                                GameLogic.MaxLegs = 7;
                                break;
                            }
                    }
                }
            }
        }
        static public PointsGameModes CurrentPointsGameMode
        {
            get
            {
                return currentPointsGameMode;
            }
            set
            {
                if (value != currentPointsGameMode)
                {
                    currentPointsGameMode = value;
                    switch (currentPointsGameMode)
                    {
                        case PointsGameModes.DoubleStartDoubleEnd:
                            {
                                GameLogic.Ends = GameLogic.Modes.Double;
                                GameLogic.Starts = GameLogic.Modes.Double;
                                break;
                            }
                        case PointsGameModes.SimpleStartDoubleEnd:
                            {
                                GameLogic.Starts = GameLogic.Modes.Simple;
                                GameLogic.Ends = GameLogic.Modes.Double;
                                break;
                            }
                        case PointsGameModes.SimpleStartSimpleEnd:
                            {
                                GameLogic.Ends = GameLogic.Modes.Simple;
                                GameLogic.Starts = GameLogic.Modes.Simple;
                                break;
                            }
                    }
                }
            }
        }
        public static bool IsPointsCurrrentGameType
        {
            get;
            set;
        }
        public static void FullScreenToggle(object _object)
        {
            graphics.ToggleFullScreen();
        }
    }
}
