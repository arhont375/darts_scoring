﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Darts
{
    abstract class Buttons
    {
        public enum alignments { Center, Left, Right };
        alignments buttonAlignment;
        public enum GameButtonStates { Normal, Pressed, Undermouse };
        protected GameButtonStates gameButtonState;

        //Texture for normal button
        protected Texture2D normalImage;

        //Texture for under mouse button
        Texture2D underMouseImage;     

        //Texture for pressed button
        Texture2D clickedImage;
        
        Vector2 position;
        Vector2 centerPosition;
        Point buttonSize;
        protected Rectangle buttonRectangle;

        //Mouse 
        protected MouseState currMouseState;

        public Buttons()
        {
        }
        // TODO: create logic to alignments
        protected Buttons(Texture2D _normalImage, Texture2D _underMouseImage, Vector2 _position, 
                      alignments _alignment = alignments.Center)
        {
            this.normalImage = _normalImage;
            if (_underMouseImage == null)
                this.underMouseImage = _normalImage;
            else
                this.underMouseImage = _underMouseImage;

            this.clickedImage = _normalImage;
            this.buttonSize = new Point(_normalImage.Width, _normalImage.Height);
            this.buttonAlignment = _alignment;
            IsUnderMouse = false;
            //Define the position by left top corner
            Position = new Vector2();
            Position = _position;
            //Define the button rectangle
            this.buttonRectangle = new Rectangle();
            this.buttonRectangle.Height = this.buttonSize.Y;
            this.buttonRectangle.Width = this.buttonSize.X;
            this.buttonRectangle.X = (int)this.position.X;
            this.buttonRectangle.Y = (int)this.position.Y;

            IsHighlighted = false;
            IsActive = true;
        }
        public virtual void CreateButton(Texture2D _normalImage, Texture2D _underMouseImage, Texture2D _clickedImage, Vector2 _position, 
                      alignments _alignment = alignments.Center)
        {
            this.normalImage = _normalImage;
            this.underMouseImage = _underMouseImage;
            this.clickedImage = _clickedImage;
            this.buttonAlignment = _alignment;
            this.buttonSize = new Point(_normalImage.Width, _normalImage.Height);
            IsUnderMouse = false;
            //Define the position by left top corner
            Position = _position;
            //Define the button rectangle
            this.buttonRectangle.Height = this.buttonSize.Y;
            this.buttonRectangle.Width = this.buttonSize.X;
            this.buttonRectangle.X = (int)this.position.X;
            this.buttonRectangle.Y = (int)this.position.Y;

            if (this.underMouseImage == null)
                this.underMouseImage = this.normalImage;
            if (this.clickedImage == null)
                this.clickedImage = this.underMouseImage;

            IsHighlighted = false;
            IsActive = true;
        }
        public void CreateButton(Texture2D _normalImage, Texture2D _clickedButton, Vector2 _position, 
                      alignments _alignment = alignments.Center)
        {
            this.CreateButton(_normalImage, null, _clickedButton, _position, _alignment);
        }

        public virtual void Update(GameTime gameTime)
        {
            currMouseState = Mouse.GetState();

            if (!IsActive)
                return;
            if (this.buttonRectangle.Contains(currMouseState.X, currMouseState.Y))
            {
                GameButtonState = GameButtonStates.Undermouse;
                if (ButtonState.Pressed == currMouseState.LeftButton)
                {
                    GameButtonState = GameButtonStates.Pressed;
                }
            }
            else
            {
                GameButtonState = GameButtonStates.Normal;
            }
        }
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                switch (GameButtonState)
                {
                    case GameButtonStates.Normal:
                        {
                            spriteBatch.Draw(normalImage, position, (IsHighlighted ? Color.Green : Color.White));
                            break;
                        }
                    case GameButtonStates.Pressed:
                        {
                            spriteBatch.Draw(clickedImage, position, Color.Red);
                            break;
                        }
                    case GameButtonStates.Undermouse:
                        {
                            spriteBatch.Draw(underMouseImage, position, Color.Green);
                            break;
                        }
                }
            }
            else
            {
                spriteBatch.Draw(normalImage, position, Color.Gray);
            }
        }

        public GameButtonStates GameButtonState
        {
            set
            {
                if (gameButtonState != value)
                    gameButtonState = value;
            }
            get
            {
                return this.gameButtonState;
            }
        }
        public bool IsHighlighted
        {
            get;
            set;
        }
        public bool IsClicked
        {
            get;
            set;
        }
        public bool IsActive
        {
            get;
            set;
        }
        public bool IsUnderMouse
        {
            get;
            set;
        }
        public Vector2 Position
        {
            get
            {
                return centerPosition;
            }
            set
            {
                if (centerPosition != value) {
                    centerPosition = value;
                    this.position.X = centerPosition.X - this.buttonSize.X / 2;
                    this.position.Y = centerPosition.Y - this.buttonSize.Y / 2;
                }
            }
        }
    }
}
