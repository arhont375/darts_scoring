﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Darts
{
    public delegate void ButtonAction(object state);
    public delegate void ButtonDelegate();

    class ActionButton : Buttons
    {
        object stateAfterClick;

        ButtonAction buttonAction;
        event ButtonDelegate buttonUnhighlightEvent;

        public ActionButton () : base()
        {
        }

        public ActionButton(Texture2D _normalImage, Texture2D _underMouseImage, Vector2 _position, ButtonAction _buttonAction, object _stateAfterClick) 
            : base(_normalImage, _underMouseImage, _position)
        {
            buttonAction = _buttonAction;
            stateAfterClick = _stateAfterClick;

            IsButtonPressed = false;
        }

        public override void Update(GameTime gameTime)
        {
            if (!base.IsActive)
            {
                return;
            }

            currMouseState = Mouse.GetState();

            if (this.buttonRectangle.Contains(currMouseState.X, currMouseState.Y))
            {
                gameButtonState = GameButtonStates.Undermouse;
                if (ButtonState.Pressed == currMouseState.LeftButton)
                {
                    gameButtonState = GameButtonStates.Pressed;
                    IsButtonPressed = true;
                }
                if (ButtonState.Released == currMouseState.LeftButton && IsButtonPressed)
                {
                    buttonAction(stateAfterClick);
                    if (buttonUnhighlightEvent != null)
                    {
                        buttonUnhighlightEvent();
                        base.IsHighlighted = true;
                    }
                    IsButtonPressed = false;
                }
            }
            else
            {
                gameButtonState = GameButtonStates.Normal;
                IsButtonPressed = false;
            }
        }        

        bool IsButtonPressed 
        {
            get;
            set;
        }
        void UnhighlightEventHandler()
        {
            base.IsHighlighted = false;
        }
        public ButtonDelegate GetEventHandler
        {
            get
            {
                return UnhighlightEventHandler;
            }
        }
        public ButtonDelegate AddEventHandler
        {
            set
            {
                buttonUnhighlightEvent += value;
            }
        }
    }
}
