﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Darts.Board
{
    class Segment : IDartsSegment
    {
        Texture2D textureSingleBig, textureSingleSmall, textureDouble, textureTriple;
        
        float angle;
        bool firstPartIsActive;
        bool secondPartIsActive;

        int cost;

        Vector2 positionSingleBig, positionSingleSmall, positionDouble, positionTriple;
        Vector2 centerPosition;

        float scope;

        enum SegmentStates { Normal, Pressed, Undermouse, InActive };
        SegmentStates segmentState = SegmentStates.Normal;

        enum SegmentParts { None, SingleSmall, Triple, SingleBig, Double };
        SegmentParts activeSegmentPart = SegmentParts.None;

        GameLogic.Modes currentMode = GameLogic.Modes.Simple;

        public event CostHandler CostSender;

        MouseState currMouseState;
       

        Segment()
        {

        }
        public Segment(Texture2D _textureSingleBig, Texture2D _textureSingleSmall, Texture2D _textureDouble, Texture2D _textureTriple,
                                Vector2 _centerPosition, float _angle, int _cost, float _scope)
        {
            angle = _angle;

            textureDouble = _textureDouble;
            textureSingleBig = _textureSingleBig;
            textureSingleSmall = _textureSingleSmall;
            textureTriple = _textureTriple;

            centerPosition = _centerPosition;
            cost = _cost;
            scope = _scope;

            positionDouble = positionSingleBig = positionSingleSmall = positionTriple = centerPosition;

            float distance;
            distance = (float) Math.Sqrt(BoardManager.RINGS[2]*BoardManager.RINGS[2] + textureSingleSmall.Width/2*textureSingleSmall.Width/2);
            distance *= scope;
            positionSingleSmall -= new Vector2((float) (distance * Math.Sin(angle + Math.PI/20)), (float) (distance * Math.Cos(angle + Math.PI/20)));


            distance = (float)Math.Sqrt(BoardManager.RINGS[3] * BoardManager.RINGS[3] + textureTriple.Width / 2 * textureTriple.Width / 2);
            distance *= scope;
            positionTriple -= new Vector2((float)(distance * Math.Sin(angle + Math.PI / 20)), (float)(distance * Math.Cos(angle + Math.PI / 20)));

            distance = (float)Math.Sqrt(BoardManager.RINGS[4] * BoardManager.RINGS[4] + textureSingleBig.Width / 2 * textureSingleBig.Width / 2);
            distance *= scope;
            positionSingleBig -= new Vector2((float)(distance * Math.Sin(angle + Math.PI / 20)), (float)(distance * Math.Cos(angle + Math.PI / 20)));

            distance = (float)Math.Sqrt(BoardManager.RINGS[5] * BoardManager.RINGS[5] + textureDouble.Width / 2 * textureDouble.Width / 2);
            distance *= scope;
            positionDouble -= new Vector2((float)(distance * Math.Sin(angle + Math.PI / 20)), (float)(distance * Math.Cos(angle + Math.PI / 20)));

            CostSender += BoardManager.BoardCostHandler;

            IsActive = true;
            firstPartIsActive = false;
            secondPartIsActive = false;
        }

        public void Update(GameTime gameTime)
        {
            currMouseState = Mouse.GetState();

            if (!IsActive)
                return;

            float mouseDistance = (float)Math.Sqrt((currMouseState.X - centerPosition.X) * (currMouseState.X - centerPosition.X) +
                                    (currMouseState.Y - centerPosition.Y) * (currMouseState.Y - centerPosition.Y));

            if (!(mouseDistance > (BoardManager.RINGS[1] * scope) && mouseDistance < (BoardManager.RINGS[5] * scope)))
            {
                SegmentState = SegmentStates.Normal;
            }
            else
            {
                Vector2 mouseCoord = new Vector2(currMouseState.X - centerPosition.X, currMouseState.Y - centerPosition.Y);
                double mouseAngle = 0;

                //Some magic
                if (mouseCoord.Y >= 0)
                {
                    if (mouseCoord.X >= 0)
                    {
                        mouseAngle = Math.Acos(mouseCoord.Y / mouseDistance) + 2 * Math.PI; //+
                    }
                    else
                    {
                        mouseAngle = -Math.Acos(mouseCoord.Y / mouseDistance) + 2 * Math.PI; // +
                    }
                }
                else
                {
                    if (mouseCoord.X >= 0) //for mouseCoord.Y < 0
                    {
                        mouseAngle = Math.Acos(mouseCoord.Y / mouseDistance) + 2 * Math.PI;
                    }
                    else
                    {
                        mouseAngle = Math.Acos(mouseCoord.X / mouseDistance) + Math.PI / 2; //+
                    }
                }


                if ((mouseAngle >= (angle - Math.PI / 20 + Math.PI)) && (mouseAngle <= (angle + Math.PI / 20 + Math.PI)) && !secondPartIsActive)
                {
                    SegmentState = SegmentStates.Undermouse;
                    firstPartIsActive = true; 
                    if (ButtonState.Pressed == currMouseState.LeftButton)
                    {
                        SegmentState = SegmentStates.Pressed;
                        IsSegmentPressed = true;

                    }
                    if (ButtonState.Released == currMouseState.LeftButton && IsSegmentPressed)
                    {
                        CostSender(cost, CurrentMode);
                        IsSegmentPressed = false;
                    }
                    ActiveSegmentPart = GetActiveSegmentPart(mouseDistance);
                }
                else
                {
                    firstPartIsActive = false;
                    if (!secondPartIsActive)
                    {
                        SegmentState = SegmentStates.Normal;
                        IsSegmentPressed = false;
                    }
                }

                if (this.cost != 20)
                {
                    return;
                }
                if (firstPartIsActive)
                {
                    return;
                }

                
                if ((mouseAngle >= (angle - Math.PI / 20 + 3 * Math.PI)) && (mouseAngle <= (angle + Math.PI / 20 + 3 * Math.PI)))
                {
                    secondPartIsActive = true;
                    SegmentState = SegmentStates.Undermouse;
                    if (ButtonState.Pressed == currMouseState.LeftButton)
                    {
                        SegmentState = SegmentStates.Pressed;
                        IsSegmentPressed = true; 
                    }
                    if (ButtonState.Released == currMouseState.LeftButton && IsSegmentPressed)
                    {
                        CostSender(cost, CurrentMode);
                        IsSegmentPressed = false;
                    }
                    ActiveSegmentPart = GetActiveSegmentPart(mouseDistance);
                }
                else
                {
                    secondPartIsActive = false;
                    SegmentState = SegmentStates.Normal;
                    IsSegmentPressed = false;
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Color color = (IsHighlighted ? Color.Yellow : Color.White);
            switch(SegmentState)
            {
                case SegmentStates.Normal: 
                    {
                        spriteBatch.Draw(textureDouble, positionDouble, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                        spriteBatch.Draw(textureSingleBig, positionSingleBig, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                        spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                        spriteBatch.Draw(textureTriple, positionTriple, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0); 
                        break;
                    }
                case SegmentStates.Pressed:
                    {
                        switch (activeSegmentPart)
                        {
                            case (SegmentParts.Double):
                                {
                                    spriteBatch.Draw(textureDouble, positionDouble, null, Color.Red, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureTriple, positionTriple, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    break;
                                }
                            case (SegmentParts.SingleBig):
                                {
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, Color.Red, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureTriple, positionTriple, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureDouble, positionDouble, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    break;
                                }
                            case (SegmentParts.SingleSmall):
                                {
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, Color.Red, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureTriple, positionTriple, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureDouble, positionDouble, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    break;
                                }
                            case (SegmentParts.Triple):
                                {
                                    spriteBatch.Draw(textureTriple, positionTriple, null, Color.Red, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureDouble, positionDouble, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    break;
                                }
                        }
                        break;
                    }
                case SegmentStates.Undermouse:
                    {
                        switch (activeSegmentPart)
                        {
                            case (SegmentParts.Double):
                                {
                                    spriteBatch.Draw(textureDouble, positionDouble, null, Color.Green, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureTriple, positionTriple, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0); 
                                    break;
                                }
                            case (SegmentParts.SingleBig):
                                {
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, Color.Green, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureTriple, positionTriple, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureDouble, positionDouble, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    break;
                                }
                            case (SegmentParts.SingleSmall):
                                {
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, Color.Green, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureTriple, positionTriple, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureDouble, positionDouble, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    break;
                                }
                            case (SegmentParts.Triple):
                                {
                                    spriteBatch.Draw(textureTriple, positionTriple, null, Color.Green, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureDouble, positionDouble, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleBig, positionSingleBig, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, color, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                                    break;
                                }
                        }
                        break;
                    }
                case SegmentStates.InActive:
                    {
                        spriteBatch.Draw(textureDouble, positionDouble, null, Color.Gray, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                        spriteBatch.Draw(textureSingleBig, positionSingleBig, null, Color.Gray, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                        spriteBatch.Draw(textureSingleSmall, positionSingleSmall, null, Color.Gray, -angle, Vector2.Zero, scope, SpriteEffects.None, 0);
                        spriteBatch.Draw(textureTriple, positionTriple, null, Color.Gray, -angle, Vector2.Zero, scope, SpriteEffects.None, 0); 
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

        }

        SegmentParts GetActiveSegmentPart(float mouseDistance)
        {
            if (mouseDistance >= BoardManager.RINGS[1] * scope && mouseDistance < BoardManager.RINGS[2] * scope)
            {
                return SegmentParts.SingleSmall;
            }
            if (mouseDistance >= BoardManager.RINGS[2] * scope && mouseDistance < BoardManager.RINGS[3] * scope)
            {
                return SegmentParts.Triple;
            }
            if (mouseDistance >= BoardManager.RINGS[3] * scope && mouseDistance < BoardManager.RINGS[4] * scope)
            {
                return SegmentParts.SingleBig;
            }
            if (mouseDistance >= BoardManager.RINGS[4] * scope && mouseDistance < BoardManager.RINGS[5] * scope)
            {
                return SegmentParts.Double;
            }
            return SegmentParts.None;
        }

        SegmentParts ActiveSegmentPart
        {
            set
            {
                if (activeSegmentPart != value)
                    activeSegmentPart = value;
            }
            get
            {
                return activeSegmentPart;
            }
        }

        public int GetCost
        {
            get
            {
                return cost;
            }
        }
        int CurrentCost
        {
            get
            {
                switch (ActiveSegmentPart)
                {
                    case (SegmentParts.Double):
                        {
                            return cost * 2;
                        }
                    case (SegmentParts.Triple):
                        {
                            return cost * 3;
                        }
                    default:
                        {
                            return cost;
                        }
                }
            }
            set
            {
                if (value != cost)
                    cost = value;
            }

        }
        GameLogic.Modes CurrentMode
        {
            get
            {
                switch (ActiveSegmentPart)
                {
                    case (SegmentParts.Double):
                        {
                            currentMode = GameLogic.Modes.Double;
                            return currentMode;
                        }
                    case (SegmentParts.Triple):
                        {
                            currentMode = GameLogic.Modes.Triple;
                            return currentMode;
                        }
                    default:
                        {
                            currentMode = GameLogic.Modes.Simple;
                            return currentMode;
                        }
                }
            }
            set
            {
                if (currentMode != value)
                {
                    currentMode = value;
                }
            }
        }
        SegmentStates SegmentState
        {
            set
            {
                if (segmentState != value)
                    segmentState = value;
            }
            get
            {
                return this.segmentState;
            }
        }
        public bool IsActive
        {
            get;
            set;
        }
        public bool IsHighlighted
        {
            get;
            set;
        }
        bool IsUndermouse
        {
            get;
            set;
        }
        public bool IsClicked
        {
            get;
            set;
        }
        bool IsSegmentPressed
        {
            get;
            set;
        }

    }
}
