﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Darts.Board
{
    class Ring : IDartsSegment
    {
        Texture2D texture;
        int cost;
        Vector2 centerPosition;
        Vector2 position;
        GameLogic.Modes mode;

        float scope;

        enum RingStates { Normal, Pressed, Undermouse, InActive };
        RingStates ringState = RingStates.Normal;

        public event CostHandler CostSender;

        MouseState currMouseState;

        public Ring(Texture2D _texture, Vector2 _centerPosition, int _cost, float _scope, GameLogic.Modes _mode = GameLogic.Modes.Simple)
        {
            texture = _texture;
            centerPosition = _centerPosition;
            Cost = _cost;
            scope = _scope;
            mode = _mode;

            position = centerPosition - new Vector2((texture.Width / 2) * scope, (texture.Height / 2) * scope);

            CostSender += BoardManager.BoardCostHandler;

            IsActive = true;
        }

        public void Update(GameTime gameTime)
        {
            currMouseState = Mouse.GetState();

            if (!IsActive)
                return;

            float mouseDistance = (float)Math.Sqrt((currMouseState.X - centerPosition.X) * (currMouseState.X - centerPosition.X) + 
                                                (currMouseState.Y - centerPosition.Y) * (currMouseState.Y - centerPosition.Y));

            Vector2 ringSizes;
            switch (texture.Width)
            {
                case 2000: // BIG RING
                    {
                        ringSizes = new Vector2(BoardManager.RINGS[5], 1000);
                        break;
                    }
                case 150: // MIDDLE RING
                    {
                        ringSizes = new Vector2(BoardManager.RINGS[0], BoardManager.RINGS[1]);
                        break;
                    }
 
                case 66: // CENTER RING
                    {
                        ringSizes = new Vector2(0, 33);
                        break;
                    }
                default:
                    {
                        ringSizes = new Vector2(0, 0);
                        break;
                    }
            }
            if (mouseDistance > (ringSizes.X * scope) && mouseDistance < (ringSizes.Y * scope))
            {
                RingState = RingStates.Undermouse;
                if (ButtonState.Pressed == currMouseState.LeftButton)
                {
                    RingState = RingStates.Pressed;
                    IsSegmentPressed = true;
                }
                if (ButtonState.Released == currMouseState.LeftButton && IsSegmentPressed)
                {
                    CostSender(Cost, mode);
                    IsSegmentPressed = false;
                }
            }
            else
            {
                RingState = RingStates.Normal;
                IsSegmentPressed = false;
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            switch (RingState)
            {
                case RingStates.Normal:
                    {
                        spriteBatch.Draw(texture, position, null, (IsHighlighted ? Color.Yellow : Color.White), 0, Vector2.Zero, scope, SpriteEffects.None, 0);
                        break;
                    }
                case RingStates.Pressed:
                    {
                        spriteBatch.Draw(texture, position, null, Color.Red, 0, Vector2.Zero, scope, SpriteEffects.None, 0); 
                        break;
                    }
                case RingStates.Undermouse:
                    {
                        spriteBatch.Draw(texture, position, null, Color.Green, 0, Vector2.Zero, scope, SpriteEffects.None, 0);
                        break;
                    }
                case RingStates.InActive:
                    {
                        spriteBatch.Draw(texture, position, null, Color.Gray, 0, Vector2.Zero, scope, SpriteEffects.None, 0);
                        break;
                    }

                default:
                    {
                        break;
                    }
            }

        }

        RingStates RingState
        {
            set
            {
                if (ringState != value)
                    ringState = value;
            }
            get
            {
                return this.ringState;
            }
        }
        int Cost
        {
            get
            {
                return cost;
            }
            set
            {
                if (value != cost)
                    cost = value;
            }
        }
        public bool IsHighlighted
        {
            get;
            set;
        }
        public bool IsClicked
        {
            get;
            set;
        }
        public bool IsActive
        {
            get;
            set;
        }
        bool IsSegmentPressed
        {
            get;
            set;
        }
    }
}
