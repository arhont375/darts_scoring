using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Darts.Board
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public delegate void CostHandler(int cost, GameLogic.Modes mode);
    
    public interface IDartsSegment
    {
        bool IsClicked { get; set; }
        bool IsActive { get; set; }
        event CostHandler CostSender;
    }



    public class BoardManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        Texture2D textureBullEye, textureCenterRing, textureDoubleRingRed, textureDoubleRingGreen, textureTripleRingRed, textureTripleRingGreen, textureBorderRing;
        Texture2D textureSingleWhiteBig, textureSingleWhiteSmall, textureSingleBlackBig, textureSingleBlackSmall;

        static Ring ringBullEye, ringCenterRing, ringBorderRing;
        static Segment []segments;
        static Segment[] sortedSegments;

        SpriteBatch spriteBatch;

        static int lastSegment = 1;

        //because image is very big
        const float SCOPE = 0.5f;
        //consts for rings on board
        static public readonly float[] RINGS = {33, 76, 440, 473, 717, 751};
        static readonly int[] COSTS = { 20, 5, 12, 9, 14, 11, 8, 16, 7, 19, 3, 17, 2, 15, 10, 6, 13, 4, 18, 1 };



        static public void BoardCostHandler(int cost, GameLogic.Modes mode)
        {
            Darts.GameLogic.CostHandler(cost, mode);
        }

        public BoardManager(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            
            //whole rings
            textureBullEye = Game.Content.Load<Texture2D>(@"images\board\bull_eye");
            textureBorderRing = Game.Content.Load<Texture2D>(@"images\board\boarder_ring");
            textureCenterRing = Game.Content.Load<Texture2D>(@"images\board\center_ring");
            //segment parts green & white
            textureDoubleRingGreen = Game.Content.Load<Texture2D>(@"images\board\double_ring_segment_green");
            textureTripleRingGreen = Game.Content.Load<Texture2D>(@"images\board\triple_ring_segment_green");
            textureSingleWhiteBig = Game.Content.Load<Texture2D>(@"images\board\big_segment_white");
            textureSingleWhiteSmall = Game.Content.Load<Texture2D>(@"images\board\little_segment_white");
            //segment parts red & black
            textureDoubleRingRed = Game.Content.Load<Texture2D>(@"images\board\double_ring_segment_red");
            textureTripleRingRed = Game.Content.Load<Texture2D>(@"images\board\triple_ring_segment_red");
            textureSingleBlackBig = Game.Content.Load<Texture2D>(@"images\board\big_segment_black");
            textureSingleBlackSmall = Game.Content.Load<Texture2D>(@"images\board\little_segment_black");
            
            Vector2 centerPosition = new Vector2(Game.Window.ClientBounds.Width / 7 * 2, Game.Window.ClientBounds.Height / 2);

            //whole rings
            ringBullEye = new Ring(textureBullEye, centerPosition, 25, SCOPE * Game1.GlobalScope, GameLogic.Modes.Double);
            ringBorderRing = new Ring(textureBorderRing, centerPosition, 0, SCOPE * Game1.GlobalScope);
            ringCenterRing = new Ring(textureCenterRing, centerPosition, 25, SCOPE * Game1.GlobalScope);
            //segments
            segments = new Segment[20];
            float angle = 0;
            for (int i = 0; i < 20; i += 2, angle += (float)Math.PI/5)
            {
                segments[i] = new Segment(textureSingleBlackBig, textureSingleBlackSmall, textureDoubleRingRed, textureTripleRingRed,
                                                    centerPosition, angle, COSTS[i], SCOPE * Game1.GlobalScope);
                segments[i + 1] = new Segment(textureSingleWhiteBig, textureSingleWhiteSmall, textureDoubleRingGreen, textureTripleRingGreen,
                                                    centerPosition, angle + (float)(Math.PI / 10), COSTS[i + 1], SCOPE * Game1.GlobalScope);
            }

            sortSegments();

            base.LoadContent();
        }
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {


            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (!Game.IsActive)
                base.Update(gameTime);

            if (Game1.CurrentGameState != Game1.GameStates.InGame)
            {
                base.Update(gameTime);
                return;
            }

            //whole rings
            ringBullEye.Update(gameTime);
            ringBorderRing.Update(gameTime);
            ringCenterRing.Update(gameTime);
            //segments
            for (int i = 0; i < 20; ++i)
                segments[i].Update(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (!Game.IsActive)
                base.Draw(gameTime);

            if (Game1.CurrentGameState != Game1.GameStates.InGame && Game1.CurrentGameState != Game1.GameStates.InGameStart 
                        && Game1.GameStates.InGameStats != Game1.CurrentGameState && Game1.CurrentGameState != Game1.GameStates.GameOver)
            {
                base.Draw(gameTime);
                return;
            }
            spriteBatch.Begin();

            //whole rings
            ringBullEye.Draw(gameTime, spriteBatch);
            ringBorderRing.Draw(gameTime, spriteBatch);
            ringCenterRing.Draw(gameTime, spriteBatch);
            //segments
            for (int i = 0; i < 20; ++i)
            {
                segments[i].Draw(gameTime, spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
        void sortSegments()
        {
            sortedSegments = new Segment[21];
            for (int i = 0; i < 20; ++i)
            {
                sortedSegments[segments[i].GetCost] = segments[i];
            }
        }
        static public void HighliteSegment(int currentSegment)
        {
            sortedSegments[lastSegment].IsHighlighted = false;
            if (currentSegment <= 20)
            {
                sortedSegments[currentSegment].IsHighlighted = true;
                lastSegment = currentSegment;
            }
            else //otherwise its last move & currentSegment == 25
            {
                ringCenterRing.IsHighlighted = true;
            }
        }
        static public void UnHighliteSegment()
        {
            sortedSegments[lastSegment].IsHighlighted = false;
            ringCenterRing.IsHighlighted = false;
        }
    }
}
