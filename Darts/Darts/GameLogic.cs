using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Darts
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GameLogic : Microsoft.Xna.Framework.GameComponent
    {
        static int currentLeg = 1;
        static int currentMove = 1;
        static int currentPlayer = 1;
        static Darts.TextBox.TextBox currentField;

        static Game1.GameTypes currentGameType = Game1.GameTypes.None;
        public enum Modes { Simple, Double, Triple };
        
        static int []playerWinners;
        static int currentWinner;
        static int currentSector;
        /// <summary>
        /// player #0 used for marks fully enclosed fields, another is obvious
        /// </summary>
        static int [][]sectorHits;
        static bool []isPlayerStart;

        static int[] playerHits;
        static int[] playerSlips;

        static int currentShot;

        public GameLogic(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            IsInitialized = false;

            MaxLegs = 1;

            playerWinners = new int[7] { 0, 0, 0, 0, 0, 0, 0 };
            playerHits = new int[7] { 0, 0, 0, 0, 0, 0, 0 };
            playerSlips = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            currentSector = 1;
            currentWinner = 0;
            currentLeg = 1;

            Starts = Modes.Simple;
            Ends = Modes.Double;

            IsLegOver = false;

            base.Initialize();
        }

        void InitGame()
        {
            currentShot = 0;
            currentPlayer = 1;
            currentMove = 1;
            GetLastPlayer = 1;

            IsMovesEnded = false;
            isPlayerStart = new bool[4 + 1] { false, false, false, false, false };
            Text.TableManager.InitTable();
            IsInitialized = true;
            
            switch (currentGameType)
            {
                case Game1.GameTypes.BigRound:
                    {
                        currentSector = 1;
                        Board.BoardManager.HighliteSegment(currentMove);
                        currentField = Text.TableManager.GetCurrentField();
                        break;
                    }
                case Game1.GameTypes.Cricket:
                    {
                        int numberOfPlayers = Game1.NumberOfPlayers;
                        sectorHits = new int[numberOfPlayers + 1][];
                        for (int i = 1; i < numberOfPlayers + 1; ++i)
                        {
                            sectorHits[i] = new int[7] { 0, 0, 0, 0, 0, 0, 0 };
                        }
                        sectorHits[0] = new int[7] { numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers };
                        break;
                    }
                case Game1.GameTypes.CricketWithPoints:
                    {
                        int numberOfPlayers = Game1.NumberOfPlayers;
                        sectorHits = new int[numberOfPlayers + 1][];
                        for (int i = 1; i < numberOfPlayers + 1; ++i)
                        {
                            sectorHits[i] = new int[7] {0, 0, 0, 0, 0, 0, 0};
                        }
                        sectorHits[0] = new int[7] { numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers, numberOfPlayers };
                        break;
                    }
                default:
                    {
                        currentField = Text.TableManager.GetCurrentField();
                        break;
                    }
            }
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        bool CurrentLegIsOver()
        {
            switch (currentGameType)
            {
                case Game1.GameTypes.BigRound:
                    {
                        if (currentSector == 25 && currentPlayer == Game1.NumberOfPlayers && currentField.ValuesNumber == 3)
                        {
                            return true;
                        }
                        break;
                    }
                case Game1.GameTypes.Cricket:
                    {
                        if (sectorHits[0].Max() == 0)
                        {
                            return true;
                        }
                        break;
                    }
                case Game1.GameTypes.CricketWithPoints:
                    {
                        if (sectorHits[0].Max() == 0)
                        {
                            return true;
                        }
                        break;
                    }
                case Game1.GameTypes.CollectPoints:
                    {
                        if (currentMove == 10 && currentField.ValuesNumber == 3)
                        {
                            return true;
                        }
                        break;
                    }
                //for Points101, Points301, Points501
                default:
                    {
                        int currentSum = Text.TableManager.GetCurrentPlayerSum();
                        if (currentSum < 0)
                        {
                            Text.TableManager.SubLastMove();
                            currentField.Text = "0";
                            currentShot = 3;
                            return false;
                        }
                        else if (currentSum == 0)
                        {
                            if (Ends == Modes.Simple)
                                return true;
                            if (LastMode != Ends)
                            {
                                Text.TableManager.SubLastMove();
                                currentShot = 3;
                                currentField.Text = "0"; 
                                return false;
                            }
                            return true;
                        }
                        break;
                    }
            }
            return false;
        }
        public override void Update(GameTime gameTime)
        {
            if (currentGameType != Game1.CurrentGameType)
            {
                currentGameType = Game1.CurrentGameType;
                switch (currentGameType)
                {
                    case Game1.GameTypes.Points101:
                        {
                            Text.TableManager.IsCricket = false;
                            Text.TableManager.SetInitScore(101);
                            break;
                        }
                    case Game1.GameTypes.Points301:
                        {
                            Text.TableManager.IsCricket = false;
                            Text.TableManager.SetInitScore(301);
                            break;
                        }
                    case Game1.GameTypes.Points501:
                        {
                            Text.TableManager.IsCricket = false;
                            Text.TableManager.SetInitScore(501);
                            break;
                        }
                    case Game1.GameTypes.BigRound:
                        {
                            Text.TableManager.IsCricket = false;
                            Text.TableManager.SetInitScore(0);
                            break;
                        }
                    case Game1.GameTypes.Cricket:
                        {
                            Text.TableManager.IsCricket = true;
                            Text.TableManager.SetInitScore(0);
                            break;
                        }
                    case Game1.GameTypes.CricketWithPoints:
                        {
                            Text.TableManager.IsCricket = true;
                            Text.TableManager.SetInitScore(0);
                            break;
                        }
                    case Game1.GameTypes.CollectPoints:
                        {
                            Text.TableManager.IsCricket = false;
                            Text.TableManager.SetInitScore(0);
                            break;
                        }
                }
            }       
            if (Game1.CurrentGameState != Game1.GameStates.InGame) 
            {
                base.Update(gameTime);
                return;
            }
            if (!IsInitialized)
            {
                InitGame();
            }

            if (CurrentLegIsOver() || IsMovesEnded)
            {
                IsLegOver = true;
                if (!IsMovesEnded)
                {
                    playerWinners[GetCurrentPlayer] += 1;
                }
                else
                {
                    currentPlayer = 0;
                }
                if (currentLeg != MaxLegs)
                {
                    Game1.CurrentGameState = Game1.GameStates.InGameStats;
                    IsInitialized = false;
                }
                else
                {
                    Game1.CurrentGameState = Game1.GameStates.GameOver;
                }

                ++currentLeg;
                IsMovesEnded = false;
            }
            if (Game1.CurrentGameState != Game1.GameStates.InGame)
            {
                base.Update(gameTime);
                return;
            }

            if (currentShot == 3)
            {
                currentShot = 0;
                GetLastPlayer = currentPlayer;
                currentPlayer += 1;
                if (currentPlayer > Game1.NumberOfPlayers)
                {
                    currentPlayer = 1;
                    currentMove += 1;
                    switch (currentGameType)
                    {
                        case Game1.GameTypes.BigRound:
                            {
                                ++currentSector;
                                if (currentSector == 21)
                                {
                                    currentSector = 25;
                                }
                                Board.BoardManager.HighliteSegment(currentSector);
                                break;
                            }
                    }

                    if (Game1.IsPointsCurrrentGameType)
                    {
                        if (currentMove > NumberOfMoves)
                        {
                            IsMovesEnded = true;
                            base.Update(gameTime);
                            return;
                        }
                    }
                    
                    Text.TableManager.HighLightCurrentMove();
                }
                if (Game1.CurrentGameType != Game1.GameTypes.Cricket && Game1.CurrentGameType != Game1.GameTypes.CricketWithPoints)
                {
                    currentField = Text.TableManager.GetNextField();
                }

                Text.TableManager.HighLightCurrentPlayer();
            }

            base.Update(gameTime);
        }

        static public void CostHandler (int cost, GameLogic.Modes mode)
        {
            ++currentShot;
            int divider = GetDivider(mode);

            switch (currentGameType)
            {
                case Game1.GameTypes.BigRound:
                    {

                        if (cost == currentSector && cost * divider != 50)
                        {
                            currentField.AddValue(cost * divider);
                            currentField.IsChanged = true;
                            ++playerHits[currentPlayer];
                        }
                        else
                        {
                            currentField.AddValue(0);
                            ++playerSlips[currentPlayer];
                        }

                        break;
                    }
                case Game1.GameTypes.Cricket:
                    {
                        int shiftedCost = GetShiftedCost(cost);

                        if (shiftedCost < 0)
                        {
                            ++playerSlips[currentPlayer];
                            break;
                        }

                        currentField = Text.TableManager.GetFieldByCost(cost);
                        Text.TableManager.SetCurrentField(currentField);
                        if (currentField.CrossAmount == 3)
                        {
                            ++playerSlips[currentPlayer];
                            break;
                        }

                        ++playerHits[currentPlayer];
                        currentField.AddCross(divider);
                        sectorHits[currentPlayer][shiftedCost] += 1;
                        if (sectorHits[currentPlayer][shiftedCost] == 3)
                        {
                            --sectorHits[0][shiftedCost];
                        }
                        break;
                    }
                case Game1.GameTypes.CricketWithPoints:
                    {
                        int shiftedCost = GetShiftedCost(cost);
                        int crossDifferences = 0;

                        if (shiftedCost < 0)
                        {
                            ++playerSlips[currentPlayer];
                            break;
                        }

                        currentField = Text.TableManager.GetFieldByCost(cost);
                        Text.TableManager.SetCurrentField(currentField);

                        if (sectorHits[currentPlayer][shiftedCost] != 1)
                        {
                            crossDifferences = (currentField.CrossAmount + divider) - 3;
                            if (currentField.AddCross(divider))
                            {
                                sectorHits[currentPlayer][shiftedCost] = 1;
                                sectorHits[0][shiftedCost] -= 1;
                                ++playerHits[currentPlayer];
                                if (crossDifferences > 0)
                                {
                                    currentField.AddValue(cost * crossDifferences);
                                    currentField.IsChanged = true;
                                }
                            }
                        }
                        else if (sectorHits[0][shiftedCost] != 0)
                        {
                            ++playerHits[currentPlayer];
                            currentField.AddValue(cost * divider);
                            currentField.IsChanged = true;
                        }
                        else
                        {
                            ++playerSlips[currentPlayer];
                        }

                        break;
                    }
                case Game1.GameTypes.CollectPoints:
                    {
                        currentField.AddValue(cost * divider);
                        currentField.IsChanged = true;
                        if (cost == 0)
                        {
                            ++playerSlips[currentPlayer];
                        }
                        else
                        {
                            ++playerHits[currentPlayer];
                        }
                        break;
                    }
                //for Points101, Points301, Points501
                default:
                    {
                        if (!isPlayerStart[currentPlayer])
                        {
                            if (!IsRightStart(mode))
                            {
                                ++playerSlips[currentPlayer];
                                currentField.AddValue(0);
                                LastMode = mode;
                                break;
                            }
                            else
                            {
                                isPlayerStart[currentPlayer] = true;
                            }
                        }

                        ++playerHits[currentPlayer];
                        currentField.AddValue(cost * divider);
                        currentField.IsChanged = true;
                        LastMode = mode;
                        break;
                    }
            }
        }

        static bool IsMovesEnded
        {
            get;
            set;
        }
        static bool IsRightStart(GameLogic.Modes _mode)
        {
            if (Starts == Modes.Simple)
            {
                return true;
            }
            else if (Starts == Modes.Double && _mode == Modes.Double)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static Modes LastMode
        {
            get;
            set;
        }
        static int GetShiftedCost(int _cost)
        {
            if (_cost >= 15 && _cost <= 20)
            {
                return _cost - 15;
            }
            else if (_cost == 25)
            {
                return 6;
            }
            else
            {
                return -1;
            }
        }
        static int GetDivider(GameLogic.Modes mode)
        {
            switch (mode)
            {
                case Modes.Double:
                    {
                        return 2;
                    }
                case Modes.Triple:
                    {
                        return 3;
                    }
                default:
                    {
                        return 1;
                    }
            }
        }

        static public int NumberOfMoves
        {
            get;
            set;
        }
        static public bool IsInitialized
        {
            get;
            set;
        }
        static public bool IsLegOver
        {
            get;
            set;
        }
        static public int MaxLegs
        {
            get;
            set;
        }
        static public Modes Ends
        {
            get;
            set;
        }
        static public Modes Starts
        {
            get;
            set;
        }
        static public void ResetShots()
        {
            currentShot = 0;
        }
        static public void SetPreviousMove()
        {
            if (currentPlayer == 1 && currentMove != 1)
            {
                currentMove -= 1;
            }

            switch (currentGameType)
            {
                case Game1.GameTypes.BigRound:
                    {
                        if (currentSector != 1)
                            currentSector -= 1;
                        else if (currentSector == 25)
                            currentSector = 20;
                        Board.BoardManager.HighliteSegment(currentSector);
                        break;
                    }
                case Game1.GameTypes.CricketWithPoints:
                    {
                        currentShot = 0;
                        currentPlayer = GetLastPlayer;
                        break;
                    }
                default:
                    {
                        currentShot = 0;
                        currentPlayer = GetLastPlayer;
                        currentField = Text.TableManager.GetCurrentField();
                        break;
                    }
            }

        }
        static public void SetPlayerWinner(int _player)
        {
            ++playerWinners[_player];
        }

        static public int GetCurrentMove
        {
            get
            {
                return currentMove;
            }
        }
        static public int GetCurrentPlayer
        {
            get
            {
                return currentPlayer;
            }
        }
        static public int GetLastPlayer
        {
            private set;
            get;
        }
        static public int GetCurrentLeg
        {
            get
            {
                return currentLeg;
            }
        }
        static public int GetCurrentWinner
        {
            private set
            {
                if (value != currentWinner)
                    currentWinner = value;
            }
            get
            {
                if (IsLegOver)
                {
                    currentWinner = playerWinners.Max();
                }
                return currentWinner;
            }
        }
        static public int GetPlayerWins(int _player)
        {
            return playerWinners[_player];
        }
        static public int GetPlayerHits(int _player)
        {
            return playerHits[_player];
        }
        static public int GetPlayerSlips(int _player)
        {
            return playerSlips[_player];
        }
    }
}
